<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'state_id', 'created_at', 'updated_at'
    ];

    public function state() {
    	return $this->belongsTo("App\State");
    }

}
