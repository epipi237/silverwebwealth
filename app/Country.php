<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'code', 'currency', 'created_at', 'updated_at'
    ];

    public function users() {
    	return $this->hasMany("App\User");
    }

    public function states() {
    	return $this->hasMany("App\State");
    }

    public function towns() {
    	return $this->hasManyThrough("App\Town", "App\State");
    }

}
