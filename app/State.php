<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'country_id', 'created_at', 'updated_at'
    ];

    public function towns() {
    	return $this->hasMany('App\Town');
    }

    public function country() {
    	return $this->belongsTo('App\Country');
    }
}
