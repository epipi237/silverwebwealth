<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'email', 'password', 'cni', 'phone', 'uplines', 'downlines', 'direct_upline', 'firstname', 'lastname'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    public function uplineSet(){
        return $this->belongsToMany('App\User', 'uplines', 'user_id', 'upline_id');
    }

    public function uplineRel(){
        return $this->hasMany('App\upline');
    }

    public function downlineSet(){
        return $this->belongsToMany('App\User', 'downlines', 'user_id', 'downline_id');
    }

    public function downlineRel(){
        return $this->hasMany('App\downline');
    }

    public function updateUplinesDownline(){
        $uplines = $this->uplines;
        if($uplines) {
            foreach ($uplines as $key => $upline) {
                $user = User::find($upline->id);
                if($user){
                    $user->downlines = array_merge([["id"=>$this->id, "level"=>$this->level-$user->level]], $user->downlines);
                    $user->save();
                }
            }
        }
    }

    public function nthLine($n=1)
    {
        $n += $this->level;
        $users = $this->downlineSet()->where('users.level', $n)->get();
        return $users;
    }

    public function firstLine()
    {
        return $this->nthLine();
    }

    public function upline()
    {
        return $this->nthUpline();
    }

    public function nthUpline($n=1)
    {
        $n = $this->level - $n;
        return $this->uplineSet()->where('users.level', $n)->first();
    }

    public function balance()
    {
        return $this->hasOne('App\userBalance');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function getUplinesAttribute($value) {
        return json_decode($value);
    }

    public function getDownlinesAttribute($value) {
        return json_decode($value);
    }

    public function setDownlinesAttribute($value) {
        $this->attributes['downlines'] = json_encode($value);
    }

    public function setUplinesAttribute($value) {
        $this->attributes['uplines'] = json_encode($value);
    }

    public function roles() {
        return $this->belongsToMany('\App\Role');
    }

    public function permissions() {
        return $this->belongsToMany('\App\Permission');
    }

    public function levels() {
        $l = $this->downlineRel()->max('level');
        if(!$l)
            return 1;
        return $l;
    }

    /**
 	 * Get User's Selected Package
 	 *
 	 * @param $this
 	 * @return resource
	 */
    public function package() {
        return $this->hasOne('\App\Package', 'id', 'package_id');
    }

    public function country() {
        return $this->belongsTo('\App\Country');
    }

    public function state() {
        return $this->belongsTo('\App\State');
    }

    public function town() {
        return $this->belongsTo('\App\Town');
    }

}
