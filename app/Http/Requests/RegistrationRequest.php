<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sponsor' => "required",
            'name' => 'required|unique:users|regex:/^[\w.-]+$/',
            "phone" => 'required',
            //"package" => 'required|exists:packages,id',
            "password" => "required|min:6",
            "password_confirmation" => "required|same:password"
        ];
    }
}
