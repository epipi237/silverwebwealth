<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\RegistrationRequest;
use App\userBalance;
use App\upline;
use App\downline;
use App\Town;
use App\State;
use App\Country;
use Mail;
use Auth;

class AuthController extends Controller {
    public $v=1;

    public function showLogin() {
    	return view('auth/login');
    }

    /*
     *
     *Get all the towns per country_id
     *
     */
    public function getTowns($country_id) {
        $states = State::whereCountryId($country_id)->get();
        $content = "<option value=''>Select your town</option>";

        $states->each(function($state) use(&$content){
            $towns = Town::whereStateId($state->id)->orderBy('name','asc')->get();
            foreach ($towns as $value) {
                $content = $content."<option value='$value->id'>$value->name</option>";   
            }
        });

        return $content;
    }

    public function showRegister($username){
    	$user = User::whereName($username)->first();

        if(!$user){
            return redirect('login')->with('auth_message', 'sponsor does not exist');
        }
    	//if(!$user || count($user->firstLine())==4) {
    	if(User::where('direct_upline', $user->id)->count() >= 4) {

            return view('auth.max_downline', compact('user'));
      		//return abort(404);
        }
        return view('auth.register', compact('user'));
    }

    public function register(RegistrationRequest $request, $username) {
        $upline_user = User::whereName($username)->first();
        if(!$upline_user || User::where('direct_upline', $upline_user->id)->count() >= 4) {
            return view('auth.max_downline', compact('upline_user'));
        }
        $upline = User::whereName($username)->first();
        $phone = $request->phone;
        if($phone[0]=="+") $phone = substr($phone, 1);
        if(strpos("237", $phone)!=0)
            $phone = "237".$phone;

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email?$request->email:$request->name;
        $user->phone = $phone;
        $user->town_id = $request->town;
        $user->country_id = $request->country;
        $user->package_id = 1;
        $user->direct_upline=$upline->id;
        $user->cni = "";
        $user->uplines = array_merge([["id"=>$upline->id, "level"=>$upline->level]], is_array($upline->uplines)?$upline->uplines:[]);
        $user->downlines = [];
        $user->level = $upline->level+1;
        $user->address = "";
        $user->password = bcrypt($request->password);

        if(!$upline_user || User::where('direct_upline', $upline_user->id)->count() >= 4) {
            return view('auth.max_downline', compact('upline_user'));
        }

        $user->save();
        $this->saveUplines($user);
        $this->updateUplinesDownline($user);
        // $i=1;
        // while($this->check($user->id)){
        //     $upline = new upline;
        //     $upline->user_id=$user->id;
        //     $upline->upline_id=$this->check($user->id);
        //     $upline->level=$i;
        //     $upline->save();
        // $i++;
        // }
        $user->attachRole('user');
        $balance = new userBalance();
        $balance->amount = 0;

        $user->balance()->save($balance);
        $user->updateUplinesDownline();

        //sending welcome email
        $data = $request->all();
        //$this->sendWelcomeEmail($data);

        Auth::login($user);
        return redirect('backoffice/profile/'.$user->id)->with("success_message", "You have successfully registered");
    }

    public function sendWelcomeEmail($data) {
        //sending a welcome mail

        Mail::send('emails.welcome', $data, function ($message) use($data){

            $message->from('info@webwealthafrica.com', 'Web Wealth Africa | '.trans('profile.email_header'));

            $message->to($data['email'])->subject(trans('profile.welcome_to')." Web Wealth Africa");
        });

        return;
    }

    public function saveUplines($user) {
        // $user=User::find($id);
        if($user->direct_upline==0 || $user->direct_upline==''){
            return false;
        }else{
            User::find($user->direct_upline)->uplineSet->each(function($item)use($user){
                $upline = new upline;
                $upline->upline_id = $item->id;
                $upline->level = $user->level - $item->level;
                $user->uplineRel()->save($upline);
            });
            $upline = new upline;
            $upline->user_id = $user->id;
            $upline->upline_id = $user->direct_upline;
            $upline->level = 1;
            $user->uplineRel()->save($upline);
        //return $user->direct_upline;
            // $upline = new upline;
            // $upline->user_id=$dd;
            // $upline->upline_id=$user->direct_upline;
            // $upline->level=$this->v;
            // $upline->save();
            // $this->v=$this->v+1;
            // $this->check($user->direct_upline,$dd);

        }
    }

    public function updateUplinesDownline($user)
    {
        $user->uplineSet->each(function($item) use($user){##
            $downline = new downline;
            $downline->downline_id = $user->id;
            $downline->level = $user->level - $item->level;
            $item->downlineRel()->save($downline);
        });
    }

    public function login(Request $request) {
        if(Auth::attempt($request->only('name',"password"), $request->remember)){
            return redirect('backoffice/profile/'.Auth::User()->id)->with("success_message", "You are loggged in");
        }else{
            return redirect()->back()->with("auth_message", "Invalid credentials");
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('login');
    }
}
