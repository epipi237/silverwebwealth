<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    function __construct()
    {
    }

    /**
     * Get users with status
     * @param void
     * @return resource
     * */
    public function index()
    {
        $users = User::where('id', '!=', Auth::user()->id)->latest()->paginate(50);
        return view('users', compact('users'));
    }

    /**
     * Change user status
     * (active, inactive, suspended)
     * @param $request
     * @return array
    */
    public function changeStatus (Request $request)
    {
        $type = $request->type;
        $user_id = $request->user_id;
        $user = User::where('id', $user_id);
        if (!$user->exists())
            return [
                'status' => 'error',
                'message' => "User doesn't seem to exists!"
            ];
        $user = $user->first();
        $user->status = $type;
        if (!$user->save())
            return [
                'status' => 'error',
                'message' => "User status could not be changed!"
            ];
        return [
            'status' => 'success',
            'user_status' => $user->status,
            'user_id' => $user->id,
            'message' => "User `". $user->name . "` status modified to `" . $user->status . "`."
        ];
    }

    /**
     * Search for user
     * @param $request
     * @return resource
    */
    function search (Request $request)
    {
        $search = $request->input('search');
        $users = User::where('name', 'like', '%'.$search.'%')
            ->orWhere('phone', 'like', '%'.$search.'%')
            ->orWhere('email', 'like', '%'.$search.'%')->paginate(50);
        Session::flash('info', "Results matching `$search`");
        if ($users->count() < 1) Session::flash('warning', "No match found!");
        return view('users', compact('users', 'search'));
    }

    /**
     * Reset User Password
     * @param $user_name
     * @param $user_id
     * @return array
     **/
    public function resetPassword (Request $request)
    {
        $user_name = $request->user_name;
        $user_id = $request->user_id;
        $user = User::where('id', $user_id);
        if (!$user->exists() || $user->first()->name !== $user_name)
            return [
                'status' => 'error',
                'message' => 'Fraud!'
            ];
        $user = $user->first();
        $user->password = bcrypt(config('app.default_password'));
        $user->save();
        return [
            'status' => 'success',
            'password' => config('app.default_password')
        ];
    }

    // togglePackageCollected
    public function togglePackageCollected ($user_id)
    {
        $user = User::find($user_id);
        if (!$user) {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found!'
            ]);
        }
        $user->package_collected = $user->package_collected? 0:1;
        if ($user->save()) {
            return response()->json([
                'status' => 'success',
                'message' => 'User package status updated successfully!!',
                'state' => $user->package_collected
            ]);
        } 
        return response()->json([
            'status' => 'error',
            'message' => 'User package status could not be updated!'
        ]);
    }
}
