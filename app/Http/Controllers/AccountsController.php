<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Auth;
use App\User;
use Session;
use Validator;

class AccountsController extends Controller {
    
    public function show(Request $request, $user_id=0) {
        if (!User::where('id', $user_id)->exists()) {
            Session::flash('warning', "Don't try me!");
            return redirect()->back();
        }
        $user = User::find($user_id);
        if($request->level){
            $downlines = $user->nthLine($request->level);
        }elseif($request->downline_id){
            $downuser = User::find($request->downline_id);
            if ($downuser) {
                $downlines = $downuser->firstLine();
            }else{
                $downlines = $user->firstLine();
            }
        }else{
            $downlines = $user->firstLine();
        }
        if(Auth::User()->canAndOwns(['read-profile'], $user, ['requireAll'=>true, 'foreignKeyName'=>'id']) || Auth::User()->hasRole(['administrator', 'superadministrator'])) {
            $transactions = $user->transactions()->orderBy('id', "desc")->paginate(20);
            $roles = $user->roles()->with('permissions')->get();
            $current_level = $request->get('level', 1);
            return view('backoffice.profile', compact('user', 'transactions', 'roles', 'downlines', 'current_level'));
        }

        Session::flash('danger', "You're not permitted to view the profile!");
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cni' => 'required',
            'email' => ['nullable', Rule::unique('users')->ignore($request->user_id)],
            'phone' => ['nullable', Rule::unique('users')->ignore($request->user_id)]
            ]);

        $phone = $request->phone;
        if($phone[0]=="+") $phone = substr($phone, 1);
        if(strpos("237", $phone)!=0) $phone = "237".$phone;

        if ($validator->fails()) {
            $inputs = "";
            $count = 1;
            $errors = $validator->errors()->messages();
            $error_count = count($errors);
            foreach ($errors as $key => $error) {
                if ($error_count === $count && $error_count > 1) {
                    $inputs .= ' and ' . $key . '.';
                }
                $inputs .= $key . ($error_count>1?', ':'.');
                ++$count;
            }
            Session::flash('danger', "Please fix the issue with the following field" . ($error_count>1?'s: ':': ') . $inputs);
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::find($request->user_id);

        if ($user->canAndOwns(['update-profile'], $user, ['requireAll'=>true, 'foreignKeyName'=>'id']) || Auth::User()->hasRole(['administrator', 'superadministrator'])) {
            $user->update([
                'firstname' => $request->firstname,
                'lastname'  => $request->lastname,
                'email'     => $request->email,
                'phone'     => $phone,
                'cni'       => $request->cni,
                'town_id'      => $request->town,
                'country_id'   => $request->country
                ]);

            Session::flash('info', "Profile information updated successfully!");
            return redirect()->back();
        }

        Session::flash('danger', "You're not permitted to edit the profile!");
        return redirect()->back();
    }

    public function resetPassword(Request $request){
        $newpass = $request->newpass;
        $cnewpass = $request->cnewpass;

        //getting the current logged in user
        $user = User::find($request->user_id);

        if($newpass==$cnewpass && \Hash::check($request->oldpass, $user->password)){
            $user->password = bcrypt($newpass);
            $user->save();
            Session::flash("success", "Your password correctly set");
            return redirect()->back();
        } else{
            Session::flash("danger", "There was an error changing your password, please make sure you enter the right old password or get to the administrator to reset it. Also, make sure the new password and it's confirmation match, thanks.");
            return redirect()->back();
        }
    }

}
