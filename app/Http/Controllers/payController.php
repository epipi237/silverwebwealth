<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\upline;
use App\userBalance;
use App\User;
use Auth;
use Session;
use App\Transaction;
class payController extends Controller
{
const first_term = 5000;
const common_ratio = 3;

    /**
 	 * Get addPayment page
 	 *
 	 * @param type
 	 * @return void
	 */
	public function getAddPayment($id)
	{
	  $user = User::find($id);
      $package = $user->package;

    return view('backoffice.add_payment', compact('user','package'));
	}

    /**
 	 * Post addPayment amount
 	 *
 	 * @param type
 	 * @return void
	 */
	public function postAddPayment($_id, Request $request)
	{
	   $user_id = $request->user_id;
       $amount = (int)$request->new_amount;
       //Verify that amount entered is valid.
       if (!is_int($amount)) {
           Session::flash('danger', "Please the amount entered is not a valid number or amount!");
           return redirect()->back();
       }
       //Verify that user is the validly clicked user.
       if ($_id !== $user_id) {
           Session::flash('danger', "User whose account you attempt to credit is corrupt!");
           return redirect()->back();
       }

       $user = User::find($user_id);
       if (!$user) {
           Session::flash('danger', "Please, the user whose account you attempt to credit is invalid!");
           return redirect()->back();
       }
       //Check if user has paid for his/her package already.
       if ($user->amount_paid >= $user->package->amount) {
           Session::flash('info', "User whose account you attempt to credit is already credited!");
           return redirect()->back();
       }
       //Check if user has 'create-payment' ability
       if ($this->hasPermission(Auth::User(), 'create-payment')) {
           $user->amount_paid = ($amount + (int) $user->amount_paid);
           $user->save();
           if ($user->amount_paid >= $user->package->amount) {
               $user->paid = "yes";
               $user->save();
           }

           Session::flash('success', "User `" . $user->name . "` account successfully credited!");
           return redirect()->back();
       }
       Session::flash('danger', "You are not authorised to add a payment whatsoever! Contact your administrator!");
       return redirect()->back();
	}

    /**
 	 * Check if user has a particular permission
 	 * Functions gets all the permissions of the user's roles.
 	 * @param $user
 	 * @param $permision_type
 	 * @return boolean
	 */
     public function hasPermission ($user, $permission_type)
     {
         $hasPermission = false;
         $user_roles = $user->roles()->with('permissions')->get();
         foreach ($user_roles as $role) {
             $__perms = $role->permissions->pluck('name')->toArray();
             if (!$hasPermission) {
                 if (in_array($permission_type, $__perms)) {
                     $hasPermission = true;
                 }
              }
         }
         return $hasPermission;
     }


    public function pay($id){
    	$userpaid=User::find($id);
    	if($userpaid->payment_done==1) {
    		return \Redirect::Back()->with('danger','something went wrong');
    	}
    $uplines=upline::where('user_id',$id)->get();
    foreach ($uplines as $key => $upline) {
    	$a=(int)$upline->level - 1;
    	$expo=pow(payController::common_ratio,$a);
    	$second=pow(4,$upline->level);
    	$amount=(payController::first_term)*($expo)/($second);
    	$userBalance=userBalance::where('user_id',$upline->upline_id)->first();
    	$userBalance->amount=$userBalance->amount+$amount;
    	$userBalance->save();

    	//die();
    	
    }
    $userpaid->payment_done='1';
      $userpaid->save();
      return redirect()->back()->with('success','finalize payment complete');
    }
    public function payment($id){
      $user=User::find($id);
     return view('backoffice.payment',compact('user'));
    }
    public function withraw(){
      $user=Auth::user();
      return view('backoffice.withdraw',compact('user'));
    }
    public function postwidthraw(Request $request){
      if(Auth::user()->paid=='no'){
                 return \Redirect::back()->with('danger','Not completed');
 
      }
    $amount=$request->amount;
    $phone=$request->phone;
    if(Auth::user()->balance->amount-$amount>=0){
      $transaction= new Transaction;
      $transaction->user_id=Auth::user()->id;
      $transaction->author_id=Auth::user()->id;
      $transaction->previous_balance=Auth::user()->balance->amount;
      $transaction->amount=$amount;
      $transaction->balance=Auth::user()->balance->amount-$amount;
      $transaction->status='completed';
      $transaction->details='';
      $transaction->save();
      Auth::user()->balance->amount=Auth::user()->balance->amount-$amount;
      if(Auth::user()->balance->save()){
        $pass=env('MOMO_AUTH_PASS', 'production');
        $amt=0.95*$amount;
        $amt=round($amt);
        $code=@file_get_contents("https://developer.mtn.cm/OnlineMomoWeb/faces/transaction/transaction.xhtml?idbouton=2&typebouton=PAIE&_amount=$amt&_tel=$phone&_clP=$pass&_email=vauvaumi%40gmail.com&submit.x=0&submit.y=0");
        $code=json_decode($code);
       // $code->StatusCode;

        if($code->StatusCode!='01'){
      Auth::user()->balance->amount=Auth::user()->balance->amount+$amount;
      Auth::user()->balance->save();
      $transaction->status='cancelled';
      $transaction->save();
        }
       return \Redirect::back()->with('success','successfully completed');

      }
      }else{
          return \Redirect::back()->with('danger','Not completed');
    
      }
      
    

    }



















}
