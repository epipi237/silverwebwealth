<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Permission;
use Illuminate\Http\Request;

class RolesAndPermissionsController extends Controller {
    //

	public function roleIndex(Request $request) {
		$roles = Role::latest()->paginate(10);
		$perms = Permission::all();

		return view('roles.index', compact('roles', 'perms'));
	}

	public function roleCreate(Request $request) {
		$perms = Permission::all();

		return view('roles.create', compact('perms'));
	}

	public function roleStore(Request $request) {
		$role = new Role();
		$role = $role->create($request->all());

    	//updating the crated roles permissions
		$role->attachPermissions(array_values($request->perms));

		return redirect()->back()->with("success", "Role successfully created");
	}

	public function roleEdit(Request $request, $id) {
		$role = Role::find($id);
		if(!$role) return redirect()->back()->with("danger", "Role not found");

		$perms = Permission::all();

		return view('roles.update', compact('role', 'perms'));
	}

	public function roleUpdate(Request $request, $id) {
		$role = Role::find($id);
		if(!$role) return redirect()->back()->with("danger", "Role not found");

		$role->update($request->all());

		if(isset($request->perms)) {
    		//removing all the role permissions and reassociating it with these news ones
			$role->detachAllPermissions();
			$role->attachPermissions(array_values($request->perms));
		}else {
			$role->detachAllPermissions();
		}

		return redirect()->back()->with("success", "Role successfully updated");
	}

	public function roleDelete(Request $request, $id) {
		$role = Role::find($id);
		if(!$role) return redirect()->back()->with("danger", "Role not found");

		$role->delete();

		return ["success" => "Role successfully deleted"];
	}

	public function roleUser(Request $request, $id) {
		$role = Role::find($id);
		if(!$role) return redirect()->back()->with("danger", "Role not found");

		$users = User::all();

		return view('roles.roles-user', compact('role', 'users'));
	}

	public function roleUserStore(Request $request, $id) {
		foreach ($request->users as $user_id) {
			$user = User::find($user_id);

			$user->attachRole($id);
		}

		return redirect()->back()->with("success", "Role successfully given to user(s)");
	}

	public function roleUserDelete(Request $request, $user_id, $role_id) {
		$user = User::find($user_id);
		if(!$user) return redirect()->back()->with("danger", "User not found");

		$user->detachRole($role_id);

		return ["success" => "Role successfully removed from user"];
	}

	public function permissionIndex(Request $request) {
		$permissions = Permission::latest()->paginate(10);

		return view('permissions.index', compact('permissions'));
	}

	public function permissionCreate(Request $request) {
		return view('permissions.create');
	}

	public function permissionStore(Request $request) {
		$perm = new Permission();
		$perm->create($request->all());

		return redirect()->back()->with("success", "Permission successfully created");
	}

	public function permissionEdit(Request $request, $id) {
		$perm = Permission::find($id);
		if(!$perm) return redirect()->back()->with("danger", "Permission not found");

		return view('permissions.update', compact('perm'));
	}

	public function permissionUpdate(Request $request, $id) {
		$perm = Permission::find($id);
		if(!$perm) return redirect()->back()->with("danger", "Permission not found");

		$perm->update($request->all());

		return redirect()->back()->with("success", "Permission successfully updated");
	}

	public function permissionDelete(Request $request, $id) {
		$perm = Permission::find($id);
		if(!$perm) return redirect()->back()->with("danger", "Permission not found");

		$perm->delete();

		return ["success" => "Permission successfully deleted"];
	}

}
