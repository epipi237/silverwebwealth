<?php     

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Transaction;
use App\payment;
use App\Package;
use App\User;
use Auth;

class MomoController extends Controller
{


	public function makePayment(Request $request)
	{
		$url = url(config('app.momo_url')."/api/v1/momo/checkout");


		$data = array(
			"email"=> config('app.momo_email'),
			"password" => config('app.momo_pass'),
			"phone" => $request->phone,
			"amount" => $request->amount
			);

		$payment=new payment();
		$payment->amount = $data['amount'];
		$payment->status = 0;
		$payment->user_id = Auth::User()->id;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$result = curl_exec ($ch);
		$error    = curl_error($ch);

		curl_close ($ch);
		
		if ($result==-1) { 
			return ['status'=> "false", "err"=> $error];
		}else{
			$resp = json_decode($result);
			if($resp && $resp->status=="success"){
				$payment->payment_id = $resp->transactionId;
				$payment->status=1;
				$payment->save();
				$user=User::find($payment->user_id);
		        $user->amount_paid=$user->amount_paid+$payment->amount;
		        $user->save();
		        $package_price=Package::find($user->package_id);
		        if($package_price->amount<=$user->amount_paid){
		        	$user->paid='yes';
		        	$user->save();
		        	$payController=new payController();
		        	$payController->pay($payment->user_id);
		        }
		     	return ["status"=>"success"];
			}else
				return $result;
		}

		return $result;

	}

	public function sendpayment(Request $request,$id){
//dd($request->phone);
		$phone=$request->phone;
		$price = (int)$request->amount;
		      // dd($phone);

		$momo_string = file_get_contents("http://api.furthermarket.com/FM/MTN/MoMo/placepayment?accountID=6398061&Phonenumber=$phone&Amount=$price");
		$array = explode(',', $momo_string);
		$array2  = explode('=', $array[1]);
		$momo_id = (int)$array2[1];

		$payment=new payment();
		$payment->amount=$price;
		$payment->status=0;
		$payment->user_id=$id;
		$payment->payment_id=$momo_id;
		$payment->save();
		return $momo_string;
	}

	public function checker($payment_id){
		$momo_string = file_get_contents("http://api.furthermarket.com/FM/MTN/MoMo/checkpayment?accountID=6398061&PaymentID=$payment_id");

		return $momo_string;
	}

	public function finall(Request $request){
		$payment=payment::where('payment_id',$request->payment_id)->first();
		if($payment->status==0){
			$payment->status=1;
			$payment->save();
         $user=User::find($payment->user_id);
         $user->amount_paid=$user->amount_paid+$payment->amount;
         $user->save();
         $package_price=Package::find($user->package_id);
         if($package_price->amount<=$user->amount_paid){
         	$user->paid='yes';
         	$user->save();
         	$payController=new payController();
         	$payController->pay($payment->user_id);
         }

		}
   return 1;
	}

}
