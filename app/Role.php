<?php

namespace App;

use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    //
    protected $fillable = ['name', 'display_name', 'description'];

    /**
     * Detach all permissions from a role
     *
     * @return object
     */
    public function detachAllPermissions()
    {
        \DB::table('permission_role')->where('role_id', $this->id)->delete();

        return $this;
    }


    /**
     * Get all role permissions
     *
     * @return object
     */
    public function permissions()
    {
      return $this->belongsToMany('\App\Permission', 'permission_role', 'role_id', 'permission_id');
    }

}
