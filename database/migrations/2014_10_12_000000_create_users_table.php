<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100)->unique();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email',50)->unique()->nullable();
            $table->string('phone',19)->unique();
            $table->string('uplines')->default("[]");
            $table->string('downlines')->default("[]");
            $table->integer('level');
            $table->integer('direct_upline');
            $table->enum('paid',["yes","no"])->default('no');
            $table->string('address');
            $table->string('cni');
            $table->enum('status',["active","inactive", "suspended"])->default('inactive');
            $table->string('password');
            $table->enum('payment_done', ['1', '0'])->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
