<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\userBalance;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        
        User::truncate();
        
        $user = new User();
        $user->name = "webwealthafrica";
        $user->email = "accounts@webwealthafrica.com";
        $user->phone = "000000000";
        $user->cni = "000000000";
        $user->package_id = 1;
        $user->uplines = '';
        $user->downlines = '';
        $user->direct_upline = 0;
        $user->level = 0;
        $user->address = "Buea";
        $user->password = bcrypt('secret');
        $user->save();
        $balance = new userBalance();
        $balance->amount = 0;
        $balance->save();
        $user->balance()->save($balance); 

        //give this last user super admin role and also assuming that the super admin role has id of one as it is the first entry in the laratrust_seeder.php file
        $role = Role::find(1);
        if($role)
            $user->attachRole($role);

        foreach ([2] as $item=>$value) {
            $user = new User();
            $user->name = "webwealthafrica-".$item;
            $user->email = "accounts@webwealthafrica-$item.com";
            $user->phone = "00000001$item";
            $user->cni = "00000001$item";
            $user->package_id = 1;
            $user->uplines = [];
            $user->downlines = [];
            $user->direct_upline = 1;
            $user->status = ["active","inactive", "suspended"][random_int(0,2)];
            $user->level = 0;
            $user->address = "Buea";
            $user->password = bcrypt('secret');
            $user->save();
            $balance = new userBalance();
            $balance->amount = 0;
            $balance->save();
            $user->balance()->save($balance);
        }

        // $this->call(UsersTableSeeder::class);
    }
}
