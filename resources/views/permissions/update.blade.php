
<div class="row" id="create">
	<div id="edit-profile" class="tab-pane">
		<section class="panel">                                          
			<div class="panel-body bio-graph-info">
				<h1> Update Permission</h1>

				<form class="form-horizontal" name="action" role="form" action="{{route('permissions-update', $perm->id)}}" method="POST" id="actionForm">                                                  
					{!!csrf_field()!!}
					<div class="form-group">
						<label class="col-lg-2 control-label">Name</label>
						<div class="col-lg-6">
							<input type="text" name="name" class="form-control" id="f-name" placeholder="" value="{{$perm->name}}" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Display Name</label>
						<div class="col-lg-6">
							<input type="text" name="display_name" class="form-control" id="l-name" placeholder=" " value="{{$perm->display_name}}" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Description</label>
						<div class="col-lg-6">
							<textarea name="description" id="" class="form-control" cols="3" rows="5" required="required">{{$perm->description}}</textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button type="submit" class="btn btn-primary">Update</button>
							<button type="reset" class="btn btn-danger">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
