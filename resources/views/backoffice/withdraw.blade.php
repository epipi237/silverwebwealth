
@extends('layouts.admin')

@section('title')
@lang('profile.title')
@stop

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-user-md"></i> @lang("profile.title")</h3>
    <ol class="breadcrumb">
      <li><a href="/backoffice"><i class="fa fa-home"></i></a></li>
      <li><i class="fa fa-money"></i>Your Balance :<span class="alert alert-success">{{Auth::user()->balance->amount }} FCFA</span></li>
    </ol>
  </div>
</div>


<div class="row">
  <!-- profile-widget -->
  <div class="col-md-12" id="afterPayment">

    </div>
    <div class="wrap-loader hide">
    <p align="center"> <img src="{{asset('img/Loading_icon.gif')}}" width="200px"> </p>
    </div>
    
<div id="makePayment" >
<p align="center">5% transaction fee for every transactions</p>
 <form align='center' class="form-group col-md-6" method="POST" onsubmit="return confirm('widthraw to this phone number?');" action="/mobile_money_cameroon/widthraw">
 {{csrf_field()}}
 <div class="form-group">
              <label class="col-lg-3 control-label">MoMo Receiver Number</label>
              <div class="col-lg-6">
                <input type="text" class="form-control" name="phone" placeholder=" " required>
             </div>
           </div>
           <br>
           <br>
           <div class="form-group">
              <label class="col-lg-3 control-label">Amount</label>
              <div class="col-lg-6">
                <input type="number" value="" class="form-control" name="amount" placeholder="amount in FCFA " required>

             </div>
           </div>
           <br>
           <br>
            <div class="form-group">
              <label class="col-lg-3 control-label">.</label>

              <div class="col-lg-6">
                   <button type="submit" class="btn btn-primary col-lg-12">Widthraw</button>
             </div>
           </div>

</form> 
</div>
 <p align="left"><img src="{{asset('img/mtn-mobile-money.png')}}" class="col-md-4 col-sm-12" height="160"></p>
</div>

<!-- page start-->


</div>
</div>
</div>
</section>
</div>
</div>

<!-- page end-->
@stop
@section('foot')
<script type="text/javascript">


var isOk=false
  var var1='';
  window.has_paid = 0;
  var $loader = $(".wrap-loader");

  function setter(PaymentID){
    var new_data={
      payment_id:PaymentID
    }

    //$loader.removeClass('hide');
     $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    $.ajax({
      url:"/mobile_money_cameroon/finalpayment",
      type: 'POST',
      data:new_data,
      dataType:'json',
      success: function(data) {
        if(data=='1'){
          window.has_paid=1;
            $loader.addClass('hide');
            $('#makePayment').addClass('hide');
            $('#afterPayment').html('<p class="btn btn-sucess">Payment sucessfully done.</p>');
            //donateComplete(data.transaction_id);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          //alert(errorThrown);
          $loader.addClass('hide');
          $('#afterPayment').html("{{ trans('page.donate.momo_donate_error0') }}.");
        },
      }); 
  }


  function checker(PaymentID){
    //$loader.removeClass('hide');

    $.ajax({
      url:"/mobile_money_cameroon/checker/"+PaymentID,
      type: 'GET',    
      success: function(data) {     
        var array3=data.split("|")
        console.log(array3);
        if(array3[0]==1){ 
          clearInterval(var1);          
          isOk=true;
          if(window.has_paid==0){
            setter(PaymentID);  
          } 

          window.has_paid=1;                       
          return false;       
        }
        else{
          if(array3[0]==4 || array3[0]==3 ){
            checker(PaymentID)
          }
          if(array3[0]==5){
            if(isOk==true)  {
              clearInterval(var1); 
              $loader.addClass('hide');
                $('#afterPayment').html('<p class="btn btn-danger">Payment Failed Try Later.</p>');
              return
            }           
            isOk=true
              // $.alert({
              //  text:'Alert!',
              //  content:"<div class='panel alert'><div class='panel-body'>Votre paiement n'a pas ete effectue due aux raisons suivantes <br><ul style='list-style:none'><li>Votre numero de telephone n'est pas un compte mobile money valid.</li><li>Vous n'avez pas assez de credit dans votre compte Mobile Money</li></ul></div></div>"
              // });     
              //alert("{{ trans('page.donate.momo_donate_error1') }}.");
            }
           }            

       },
       error: function (jqXHR, textStatus, errorThrown) {
          //alert(errorThrown);
        },
      });

  }

  function makePayment(e){
    e.preventDefault();
    that = e.target;
   // alert(that.action)
    isOk=false;
    window.has_paid = 0;
    $loader.removeClass('hide');
    $.ajax({
      url: that.action,
      type: 'GET',
      data: $(that).serialize(),  
      success: function(data) {

        console.log(data);
        if(data.indexOf('pending')!=-1){      
          return;
        }
        
        var array=data.split(",")
        var array2=array[1].split("=")
        var PaymentID=Number(array2[1])

        if(array[0]==1){
          var1=setInterval("checker("+PaymentID+")",2000);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
       // alert(JSON.stringify(errorThrown))
        //alert("{{ trans('page.error_reaching_server') }}.");
      }   

    });
  }


</script>
        @endsection
