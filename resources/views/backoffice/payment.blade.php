
@extends('layouts.admin')

@section('title')
@lang('profile.title')
@stop

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-user-md"></i> @lang("profile.title")</h3>
    <ol class="breadcrumb">
      <li><a href="/backoffice"><i class="fa fa-home"></i></a></li>
      <li><i class="fa fa-money"></i>Amount left :<span class="alert alert-success">{{ Auth::user()->package->amount - Auth::user()->amount_paid }} FCFA</span></li>
    </ol>
  </div>
</div>


<div class="row">
  <!-- profile-widget -->
  <div class="col-md-12" id="afterPayment">

    </div>
    <div class="wrap-loader hide">
    <p align="center"> <img src="{{asset('img/Loading_icon.gif')}}" width="200px"> </p>
    </div>
    
<div id="makePayment" >

 <form align='center' class="form-group col-md-6" method="POST" onsubmit="makePayment(event)" action="/mobile_money_cameroon/sendpayment/{{$user->id}}">
 <div class="form-group">
              <label class="col-lg-3 control-label">MoMo Number</label>
              <div class="col-lg-6">
                <input type="text" class="form-control" name="phone" placeholder=" " required>
             </div>
           </div>
           <br>
           <br>
           <div class="form-group">
              <label class="col-lg-3 control-label">Amount</label>
              <div class="col-lg-6">
                <input type="number" value="{{old('lastname')?old('lastname'):$user->lastname}}" class="form-control" name="amount" placeholder=" " required>

             </div>
           </div>
           <br>
           <br>
            <div class="form-group">
              <label class="col-lg-3 control-label">.</label>

              <div class="col-lg-6">
                   <button type="submit" class="btn btn-primary col-lg-12">Pay</button>
             </div>
           </div>

</form> 
</div>
 <p align="left"><img src="{{asset('img/mtn-mobile-money.png')}}" class="col-md-4 col-sm-12" height="160"></p>
</div>

<!-- page start-->


</div>
</div>
</div>
</section>
</div>
</div>

<!-- page end-->
@stop

@section('foot')
<script type="text/javascript">
    //$loader.removeClass('hide');
  $.ajaxSetup({
      headers:
      { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
  });

  var makePayment = function(e){
    e.preventDefault();
    var $loader = $(".wrap-loader").removeClass('hide');

    $.ajax({
      url: e.target.action,
      method: 'get',
      data: $(e.target).serialize(),
      dataType: 'json',
      success: function (data) {
        if(data.status=='success'){
          $loader.addClass('hide');
          $('#makePayment').addClass('hide');
          $('#afterPayment').html('<p class="btn btn-sucess">Payment Successful.</p>');
        }else{
          $loader.addClass('hide');
          $('#afterPayment').html('<p class="btn btn-danger">'+data.message+'</p>');
        }
      },
      error: function (error) {
          $loader.addClass('hide');
          $('#afterPayment').html('<p class="btn btn-danger">Payment Failed. Please try again</p>');
      }
    })
  }
</script>

@stop
