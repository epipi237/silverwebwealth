@extends('layouts.admin')
@section('head')

@endsection
@section('content')
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="text-center">Add Payment For User: <strong>{{ $user->name }} - ({{ $user->firstname . ' ' . $user->lastname }})</strong></h3>
        </div>
      </div>
      <table class="table table-hover table-striped">
        <tr>
          <th>Package</th>
          <td>{{$package->display_name}}</td>
        </tr>
        <tr>
          <th>Total Amount</th>
          <td>{{$package->amount}}</td>
        </tr>
        <tr>
          <th>Amount Paid</th>
          <td>{{$user->amount_paid}}</td>
        </tr>
        <tr>
          <th colspan="2"><img src="{{$package->logo}}" class="img img-thumbnail" height="70px" width="70px" alt=""></th>
        </tr>

      </table>
      @unless ($user->amount_paid >= $package->amount)
        <form class="add-payment-form" action="{{route('post_add_payment', $user->id)}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="user_id" value="{{$user->id}}">
          <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon">Balance: {{ (int)$package->amount - (int)$user->amount_paid }} XAF</span>
              <input type="text" required name="new_amount" class="form-control" value="" placeholder="Enter amount...">
            </div>
          </div>
          <div class="pull-right">
            <button type="submit" class="btn btn-success new_amount_button" >Pay Now <i class="fa fa-check"></i></button>
          </div>
        </form>
      @endunless
      <div class="pull-left">
        <a href="{{route('user_index')}}" class="btn btn-warning" >Back <i class="fa fa-reply"></i></a>
        <a href="{{route('show_profile', $user->id)}}" class="btn btn-info" >Profile <i class="fa fa-user"></i></a>
      </div>
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
  </div>
  <hr>
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
      @if ($user->amount_paid >= $package->amount)
<form action="/backoffice/pay/{{$user->id}}" method="post">
{{csrf_field()}}
  <button class="btn btn-primary btn-block">
    Finalize Payment <i class="fa fa-calender-check"></i>>
  </button>
</form>



        
      @endif
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"></div>
  </div>
@endsection
@section('foot')
  <script type="text/javascript">
    $(function () {
      $('button.new_amount_button').click(function (evt) {
        evt.preventDefault();
        var $new_amount = parseInt($('input[name="new_amount"]').val());
        var form = $('form.add-payment-form');
        if (isNaN($new_amount) || $new_amount < 0) {
          swal("Invalid amount", "Only numbers are allowed (without spaces..)!", 'error');
          return false;
        }
        $(form).submit();
      });
    });
  </script>
@endsection
