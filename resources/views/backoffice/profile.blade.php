@extends('layouts.admin')

@section('title')
@lang('profile.title')
@stop

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h3 class="page-header"><i class="fa fa-user-md"></i> @lang("profile.title")</h3>
    <ol class="breadcrumb">
      <li><a href="/backoffice"><i class="fa fa-home"></i></a></li>
      <li><i class="fa fa-user-md"></i>@lang("profile.title")</li>
    </ol>
  </div>
</div>

<div class="row">
  <!-- profile-widget -->
  <div class="col-lg-12">
    <div class="profile-widget profile-widget-info">
      <div class="panel-body">
        <div class="col-lg-2 col-sm-2">
          <br>
          <h4>{{ $user->firstname?ucfirst($user->firstname)." ".ucfirst($user->lastname): strtoupper($user->name) }}</h4>
          <div class="follow-ava">
            <img src="/img/avatar.png" alt="">
          </div>
        </div>

        <div class="col-lg-4 col-sm-4 follow-info">
          <quote>@lang("profile.welcome",['name'=>ucfirst($user->name)])</quote>
          <p> {{"@".$user->name}}</p>
          
          <p>
            <button id="copyLink" class="btn btn-xs btn-default">
              <i class="fa fa-clipboard fa_custom" aria-hidden="true"></i> {{ trans('profile.copy_your_link') }}
            </button>
            <span id='message'></span>
          </p>
          <p>
            <button id="resetPassword" class="btn btn-xs btn-default" onclick="launchResetPasswordModal({{ $user->id }})">
              <i class="fa fa-eye fa_custom" aria-hidden="true"></i> {{ trans('profile.reset_your_password') }}
            </button>
            <span id='message'></span>
          </p>
          <br>

          <h6>
            <span><i class="icon_clock_alt"></i>{{date("H:i:s a")}}</span>
            <span><i class="icon_calendar"></i>{{date("d/m/Y")}}</span>
            <span><i class="icon_pin_alt"></i>CM</span>
          </h6>
        </div>
        <div class="col-lg-2 col-sm-6 follow-info weather-category">
          <ul>
            <li class="active">

              <i class="fa fa-arrow-down fa-2x"> </i><br>
              {{count($user->downlineSet)}} {{trans_choice('profile.downlines_text', count($user->downlineSet))}}
            </li>

          </ul>
        </div>
        <div class="col-lg-2 col-sm-6 follow-info weather-category">
          <ul>
            <li class="active">

              <i class="fa fa-money fa-2x"> </i><br>
              @if(Auth::User()->hasRole('administrator') || Auth::User()->hasRole('superadministrator'))
                {{round($user->balance->amount, 2, PHP_ROUND_HALF_DOWN)}} XAF @lang('profile.inaccount')
              @else
                {{$user->paid=='no'?0:round($user->balance->amount, 2, PHP_ROUND_HALF_DOWN)}} XAF @lang('profile.inaccount')
              @endif
            </li>

          </ul>
        </div>
        <div class="col-lg-2 col-sm-6 follow-info weather-category">
          <ul>
            <li class="active">

              <i class="fa fa-arrow-up fa-2x"> </i><br>
              @lang('profile.uplines_text'): {{$user->upline()?$user->upline()->name:""}}
            </li>

          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- page start-->
<div class="row">
 <div class="col-lg-12">
  <section class="panel">
    <header class="panel-heading tab-bg-info">
      <ul class="nav nav-tabs">
        <li class="active col-xs-12 col-sm-6 col-md-2 col-lg-3">
          <a data-toggle="tab" href="#profile" style="padding:10px 0;">
            <i class="icon-user"></i>
            @lang('profile.profile_tab')
          </a>
        </li>
        <li class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
          <a data-toggle="tab" href="#edit-profile" style="padding:10px 0;">
            <i class="icon-envelope"></i>
            @lang('profile.edit_profile')
          </a>
        </li>
        <li class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
          <a data-toggle="tab" href="#recent-activity" style="padding:10px 0;">
            <i class="icon-home"></i>
            @lang('profile.recent_transactions')
          </a>
        </li>
        @role('superadministrator','administrator')
        <li class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
         <a data-toggle="tab" href="#view-roles" style="padding:10px 0;">
           <i class="icon-home"></i>
           @lang('profile.view_roles')
         </a>
       </li>
       @endrole
       <li  class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
        <a data-toggle="tab" href="#view-downline" style="padding:10px 0;">
          <i class="icon-home"></i>
          @lang('profile.view_downline')
        </a>
      </li>
      @role('superadministrator','administrator')
      <li class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
        <a data-toggle="tab" href="#view-upline" style="padding:10px 0;">
          <i class="icon-home"></i>
          @lang('profile.view_upline')
        </a>
      </li>
      @endrole
    </ul>
  </header>
  <div class="panel-body">
    <div class="tab-content">
      <div id="recent-activity" class="tab-pane ">
        <div class="profile-activity">
          @foreach($transactions as $transaction)
          <div class="act-time">
            <div class="activity-body act-in">
              <span class="arrow"></span>
              <div class="text">
                <a href="#" class="activity-img"><i class="fa fa-money fa-3x"> </i></a>
                <p class="attribution"><a href="#">{{round($transaction->amount, 2, PHP_ROUND_HALF_DOWN)}} XAF</a> @ {{date("H:i, D d - M - Y", strtotime($transaction->updated_at))}}</p>
                <p style="padding-left:52px;">
                  @lang('profile.previous_balance'): {{$transaction->previous_balance}}<br>
                  @lang('profile.current_balance'): {{$transaction->current_balance}}<br>
                  {{$transaction->details}}
                </p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>

      <!-- profile -->
      <div id="profile" class="tab-pane active">
        <section class="panel">
          <div class="bio-graph-heading" style="padding:40px 50px">
            @lang('profile.quote')
          </div>
          <div class="panel-body bio-graph-info">
            <h1>Bio Graph</h1>
            <div class="row">
              <div class="bio-row">
                <p><span>@lang("profile.firstname") </span>: <span style="width: 50%;">{{$user->firstname}} </span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.lastname")</span>: <span style="width: 50%;">{{$user->lastname}}</span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.joined")</span>: <span style="width: 50%;">{{date("D d - M - Y", strtotime($user->created_at))}}</span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.email") </span>: <span style="width: 50%;">{{$user->email}}</span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.phone") </span>: <span style="width: 50%;">{{$user->phone}}</span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.cni") </span>:  <span style="width: 50%;">{{$user->cni}}</span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.address") </span>: <span style="width: 50%;">{{$user->town?$user->town->name:''}}, {{$user->country?$user->country->name:''}}</span></p>
              </div>
              <div class="bio-row">
                <p><span>@lang("profile.paid") </span>: <span class="badge badge-danger" ><b>{{strtoupper($user->paid)}}</b></span></p>
              </div>
            </div>
          </div>
        </section>
        <section>
          <div class="row">
          </div>
        </section>
      </div>

      <!-- edit-profile -->
      <div id="edit-profile" class="tab-pane">
        <section class="panel">
          <div class="panel-body bio-graph-info">
            <h1> Profile Info</h1>
            <form class="form-horizontal" role="form" action="{{route('update_profile', $user->id)}}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="user_id" value="{{$user->id}}">
              <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                <label class="col-lg-2 control-label">First Name</label>
                <div class="col-lg-6">
                  <input type="text" value="{{$user->firstname}}" class="form-control" name="firstname" placeholder=" " required>
                  @if ($errors->has('firstname'))
                  <span class="help-block">
                   <strong>{{ $errors->first('firstname') }}</strong>
                 </span>
                 @endif
               </div>
             </div>
             <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
              <label class="col-lg-2 control-label">Last Name</label>
              <div class="col-lg-6">
                <input type="text" value="{{old('lastname')?old('lastname'):$user->lastname}}" class="form-control" name="lastname" placeholder=" " required>
                @if ($errors->has('lastname'))
                <span class="help-block">
                 <strong>{{ $errors->first('lastname') }}</strong>
               </span>
               @endif
             </div>
           </div>
           <div class="form-group{{ $errors->has('cni') ? ' has-error' : '' }}">
            <label class="col-lg-2 control-label">CNI</label>
            <div class="col-lg-6">
              <input type="text" value="{{old('cni')?old('cni'):$user->cni}}" class="form-control" name="cni" placeholder=" " required>
              @if ($errors->has('cni'))
              <span class="help-block">
               <strong>{{ $errors->first('cni') }}</strong>
             </span>
             @endif
           </div>
         </div>
         <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <label class="col-lg-2 control-label">Email</label>
          <div class="col-lg-6">
            <input type="text" id='email' value="{{old('email')?old('email'):$user->email}}" class="form-control" name="email" placeholder=" " required>
            @if ($errors->has('email'))
            <span class="help-block">
             <strong>{{ $errors->first('email') }}</strong>
           </span>
           @endif
         </div>
       </div>
       <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
        <label class="col-lg-2 control-label">Mobile</label>
        <div class="col-lg-6">
          <input type="text" value="{{old('phone')?old('phone'):$user->phone}}" class="form-control" name="phone" placeholder=" " required>
          @if ($errors->has('phone'))
          <span class="help-block">
           <strong>{{ $errors->first('phone') }}</strong>
         </span>
         @endif
       </div>
     </div>
     <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </div>
  </form>
</div>
</section>
</div>
<!-- View Roles -->
@role('superadministrator','administrator')
<div id="view-roles" class="tab-pane ">
  @foreach ($roles as $key => $role)
  <a href="">
   <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3"
   style="box-shadow: 2px 1px 3px 1px #0c7cd5; margin: 5px; min-width: 220px; min-height: 70px;padding: 0; cursor: pointer;">
   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="">
    <div class="row text-center">
     <b style="box-shadow: 2px 1px 3px 1px #0c7cd5;">{{ $role->display_name }}</b>
   </div>
   <div class="row text-center">
     @foreach ($role->permissions as $key => $permission)
     <div class="" style="color:#688a7e; font-weight:bolder;">
       {{ $permission->display_name }}
     </div>
     @endforeach
   </div>
 </div>
</div>
</a>
@endforeach
</div>
@endrole
<div id="view-downline" class="tab-pane ">
  <table class="table table-hover table-bordered table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Contact</th>
        <th>Level</th>
        <th>Paid</th>
        <th># downlines</th>
        <th> View Downline </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($downlines as $key => $downline)
      <tr>
        <td>{{$downline->name}}</td>
        <td>Tel:{{$downline->phone}}<br/> Email:{{$downline->email}}</td>
        <td>{{$downline->level-$user->level}}</td>
        <td><span class="badge" style="background:{{($downline->package->amount-$downline->amount_paid)<=0?'#00a0df':'#ff0000'}}">{{($downline->package->amount-$downline->amount_paid)<=0?"Yes":"No"}}</span></td>
        <td>{{$downline->downlineSet()->count()}}</td>
        <td>   <a href="{{route('show_profile', $user->id)}}?downline_id={{$downline->id}}#view-downline" class="btn"><i class="fa fa-sitemap"></i></a> </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <div class="btn-group">
    @foreach (range(1, $user->levels()) as $level)
    <a href="{{route('show_profile', $user->id)}}?level={{$level}}#view-downline" class='btn {{$current_level==$level?'':'btn-primary'}}'>{{$level}}</a>
    @endforeach
  </div>
</div>
@role('superadministrator','administrator')

<div id="view-upline" class="tab-pane ">
  <table class="table table-hover table-bordered table-striped">
    <thead>
      <tr>
        <th>Name</th>
        <th>Level</th>
        <th># downlines</th>
        <th> View Downline </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($user->uplineSet as $key => $upline)
      <tr>
        <td>{{$upline->name}}</td>
        <td>{{$user->level-$upline->level}}</td>
        <td>{{$upline->downlineSet()->count()}}</td>
        <td>   <a href="{{route('show_profile', $user->id)}}?downline_id={{$upline->id}}#view-downline" class="btn"><i class="fa fa-sitemap"></i></a> </td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>
@endrole

</div>
</div>
</div>
</section>
</div>
</div>

<style type="text/css">
  .form-group {
    margin-bottom: 0px;
  }
</style>

<!--Modal reset-password type start-->
<div id="reset-password-modal" tabindex="-1" role="dialog" aria-labelledby="modal-login-label" aria-hidden="true" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content col-md-10 col-md-offset-1">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
        <h4 id="modal-login-label" class="modal-title"><a href="#"><i class="fa fa-lock"></i></a> Reset Password</h4>
      </div>
      <div class="modal-body" id="reset-password_body">
        <div class="form" style="height: inherit;">
        <form class="form-horizontal row" action="{{ route('reset_password') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" id='user_id' name="user_id" value=""/>

            <div class="form-group">
              <label for="newpass" class="col-md-12 hide control-label">Old Password</label>

              <div class="col-md-12">
                <input id="oldpass" type="password" placeholder="Old Password" class="form-control input-lg c-square" name="oldpass" required>
              </div>
            </div>

            <div class="form-group">
              <label for="newpass" class="col-md-12 hide control-label">New Password</label>

              <div class="col-md-12">
                <input id="newpass" type="password" placeholder="New Password" class="form-control input-lg c-square" name="newpass" required>
              </div>
            </div>

            <div class="form-group">
              <label for="cnewpass" class="col-md-12 hide control-label">Confirm Password</label>

              <div class="col-md-12">
                <input id="cnewpass" placeholder="Confirm New Password" type="password" class="form-control input-lg c-square" name="cnewpass" required>
              </div>
            </div>

            <div class="form-group col-md-12">
              <input type="submit" class="btn btn-lg btn-primary pull-right" value="Reset Password"/>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!--END MODAL reset-password type-->

<!-- page end-->
@stop

@section('foot')
<script type="text/javascript">

  function doCopyLinkToClipboard() {

    //using the search field on the top bar as the temporal storage for the link to be copied
    var temp = document.getElementById('search');
    var tempV = temp.value;

    var link = "{{ route('register', \Auth::user()->name) }}";

    temp.value = link;

    target = temp;
    temp.parentNode.appendChild(target);

    target.focus();
    target.select();

    // copy the selection
    var succeed = false;
    try {
      succeed = document.execCommand("copy");
    } catch(e) {
      succeed = false;
    }

    msgElem = document.getElementById('message');
    msgElem.innerHTML = "{{ trans('profile.link_copied_message') }}";

    //restoring the old value of the search field used as temporal storage
    temp.value = tempV;

    setTimeout(function() {
      msgElem.innerHTML = "";
    }, 2000);

    return succeed;
  }

  $(function () {
    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
      $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
    } //add a suffix

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
      window.location.hash = e.target.hash;
    });

    $("#copyLink").on('click', function() {
      doCopyLinkToClipboard();
    });

  });

  function launchResetPasswordModal(id) {
    $("#user_id").attr("value", id);
    $("#reset-password-modal").modal();
  }

</script>
@endsection
