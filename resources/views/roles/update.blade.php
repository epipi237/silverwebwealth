
<div class="row" id="create">
	<div id="edit-profile" class="tab-pane">
		<section class="panel">                                          
			<div class="panel-body bio-graph-info">
				<h1> Update Role</h1>

				<form class="form-horizontal" name="action" role="form" action="{{route('roles-update', $role->id)}}" method="POST" id="actionForm">
					{!!csrf_field()!!}
					<div class="form-group">
						<label class="col-lg-2 control-label">Name</label>
						<div class="col-lg-6">
							<input type="text" name="name" class="form-control" id="f-name" placeholder="" value="{{$role->name}}" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Display Name</label>
						<div class="col-lg-6">
							<input type="text" name="display_name" class="form-control" id="l-name" placeholder=" " value="{{$role->display_name}}" required="required">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Description</label>
						<div class="col-lg-6">
							<textarea name="description" id="" class="form-control" cols="3" rows="5" required="required">{{$role->description}}</textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-lg-2 control-label">Permissions</label>
						<div class="col-lg-6">
							<div class="selectBox" onclick="showCheckboxes()">
								<select class="form-control">
									<option>Select Permissions</option>
								</select>
								<div class="overSelect"></div>
							</div>
							<div id="checkboxes">
								@foreach($perms as $perm)
								<label for="{{$perm->id}}" class="col-sm-3">
									<input name="perms[]" type="checkbox" value="{{$perm->id}}" id="{{$perm->id}}" @if($role->hasPermission($perm->name)) checked="checked" @endif />{{$perm->display_name}}
								</label>
								@endforeach
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<button type="submit" class="btn btn-primary">Update</button>
							<button type="reset" class="btn btn-danger">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
