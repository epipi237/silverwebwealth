@extends('layouts.admin')

@section('content')   

<style type="text/css">
	.multiselect {
		width: 200px;
	}

	.selectBox {
		position: relative;
	}

	.selectBox select {
		width: 100%;
		font-weight: bold;
	}

	.overSelect {
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
	}

	#checkboxes {
		display: none;
		border: 1px #dadada solid;
	}

	#checkboxes label {
		display: block;
	}

	#checkboxes label:hover {
		background-color: #1e90ff;
	}
</style>

<!--main content start-->
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header"><i class="fa fa-user-md"></i> Roles - Users</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="/home">Home</a></li>
			<li><i class="icon_documents_alt"></i>Roles</li>
			<li><i class="fa fa-user-md"></i>Users</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				Roles
			</header>

			<table class="table table-striped table-advance table-hover">
				<tbody>
					<tr>
						<th><i class="icon_profile"></i> Name</th>
						<th><i class="icon_calendar"></i> Display Name</th>
						<th><i class="icon_mail_alt"></i> Description</th>
						<th><i class="fa fa-user-md"></i> Users</th>
					</tr>
					@foreach($role->users as $user)
					<tr>
						<td>{{$user->name}}</td>
						<td>{{$user->display_name}}</td>
						<td>{{$user->description}}</td>
						<td>
							<div class="btn-group">
								<a class="btn btn-danger" href="#" onclick="deleteIt(this, {{$user->id}})"><i class="icon_close_alt2"></i></a>
							</div>
						</td>
					</tr>     
					@endforeach                     
				</tbody>
			</table>
		</section>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="/home">Home</a></li>
			<li><i class="icon_documents_alt"></i>Roles</li>
			<li><i class="fa fa-user-md"></i>Action</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12" id="create">
		<div id="edit-profile" class="tab-pane">
			<section class="panel">                                          
				<div class="panel-body bio-graph-info">
					<h1> Assign Roles</h1>

					<form class="form-horizontal" name="action" role="form" action="{{route('role-user-store', $role->id)}}" method="POST">
						{!!csrf_field()!!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Role</label>
							<div class="col-lg-6">
								<label name="role" class="form-control" id="role" placeholder="" required="required">
									{{$role->display_name}}
								</label>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Users</label>
							<div class="col-lg-6">
								<div class="selectBox" onclick="showCheckboxes()">
									<select class="form-control">
										<option>Select Users</option>
									</select>
									<div class="overSelect"></div>
								</div>
								<div id="checkboxes">
									@foreach($users as $user)
									<label for="{{$user->id}}" class="col-sm-3">
										<input name="users[]" type="checkbox" id="{{$user->id}}" value="{{$user->id}}"/>{{$user->name}}
									</label>
									@endforeach
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary">Save</button>
								<button type="reset" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
	function deleteIt(that, id) {
		if(confirm("Are you sure you want to delete this role?")){
			$.ajax({
				url:'/backoffice/role-users/delete/'+id+'/'+{{$role->id}},
				method: 'POST',
				dataType:'json',
				success:function(data) {
					$(that).parents('tr').fadeOut('fast');
					alert(data.success);
				},
				error:function(error) {
					//alert("Error reaching server, try again later.");
					alert(error);
				}
			});
		}
	}
	var expanded = false;

	function showCheckboxes() {
		var checkboxes = document.getElementById("checkboxes");
		if (!expanded) {
			checkboxes.style.display = "block";
			expanded = true;
		} else {
			checkboxes.style.display = "none";
			expanded = false;
		}
	}
</script>

@endsection
