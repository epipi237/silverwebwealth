@extends('layouts.admin')

@section('content')   

<style type="text/css">
	.multiselect {
		width: 200px;
	}

	.selectBox {
		position: relative;
	}

	.selectBox select {
		width: 100%;
		font-weight: bold;
	}

	.overSelect {
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
	}

	#checkboxes {
		display: none;
		border: 1px #dadada solid;
	}

	#checkboxes label {
		display: block;
	}

	#checkboxes label:hover {
		background-color: #1e90ff;
	}
</style>

<!--main content start-->
<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header"><i class="fa fa-user-md"></i> Roles</h3>
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="/home">Home</a></li>
			<li><i class="icon_documents_alt"></i>Roles</li>
			<li><i class="fa fa-user-md"></i>Index</li>
			<a href='javascript:;' onclick="create()" class="btn btn-xs btn-primary pull-right">Create</a>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
				Roles
			</header>

			<table class="table table-striped table-advance table-hover">
				<tbody>
					<tr>
						<th><i class="icon_profile"></i> Name</th>
						<th><i class="icon_calendar"></i> Display Name</th>
						<th><i class="icon_mail_alt"></i> Description</th>
						<th><i class="fa fa-user-md"></i> Users</th>
					</tr>
					@foreach($roles as $role)
					<tr>
						<td>{{$role->name}}</td>
						<td>{{$role->display_name}}</td>
						<td>{{$role->description}}</td>
						<td>
							<a class="btn btn-success" href="{{route('role-users', $role->id)}}" ><i class="fa fa-user-md"></i></a>
						</td>
						<td>
							<div class="btn-group">
								<a class="btn btn-success" href="#" onclick="loadEditor({{$role->id}})"><i class="icon_check_alt2"></i></a>
								<a class="btn btn-danger" href="#" onclick="deleteIt(this, {{$role->id}})"><i class="icon_close_alt2"></i></a>
							</div>
						</td>
					</tr>     
					@endforeach                     
				</tbody>
			</table>
			<span class="pull-right"> {{$roles->links()}} </span>
		</section>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<ol class="breadcrumb">
			<li><i class="fa fa-home"></i><a href="/home">Home</a></li>
			<li><i class="icon_documents_alt"></i>Roles</li>
			<li><i class="fa fa-user-md"></i>Action</li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-lg-12" id="create">
		<div id="edit-profile" class="tab-pane">
			<section class="panel">                                          
				<div class="panel-body bio-graph-info">
					<h1> Create Role</h1>

					<form class="form-horizontal" name="action" role="form" action="{{route('roles-store')}}" method="POST" id="actionForm">
						{!!csrf_field()!!}
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
							<div class="col-lg-6">
								<input type="text" name="name" class="form-control" id="f-name" placeholder=" " required="required">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Display Name</label>
							<div class="col-lg-6">
								<input type="text" name="display_name" class="form-control" id="l-name" placeholder=" " required="required">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Description</label>
							<div class="col-lg-6">
								<textarea name="description" id="" class="form-control" cols="3" rows="5" required="required"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Permissions</label>
							<div class="col-lg-6">
								<div class="selectBox" onclick="showCheckboxes()">
									<select class="form-control">
										<option>Select Permissions</option>
									</select>
									<div class="overSelect"></div>
								</div>
								<div id="checkboxes">
									@foreach($perms as $perm)
									<label for="{{$perm->id}}" class="col-sm-3">
										<input name="perms[]" type="checkbox" id="{{$perm->id}}" value="{{$perm->id}}"/>{{$perm->display_name}}
									</label>
									@endforeach
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" class="btn btn-primary">Save</button>
								<button type="reset" class="btn btn-danger">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">

	function create() {
		$.ajax({
			url:'/backoffice/roles/create/',
			dataType: 'html',
			success: function(data) {
				$("#create").html(data);
				$('html, body').animate({
					scrollTop: $("#create").offset().top
				}, 1000);
			},
			error: function() {
				alert('Error reaching server. Try again later.');
			}
		});
	}

	function loadEditor(id) {
		$.ajax({
			url:'/backoffice/roles/edit/'+id,
			dataType: 'html',
			success: function(data) {
				$("#create").html(data);
				$('html, body').animate({
					scrollTop: $("#create").offset().top
				}, 1000);
			},
			error: function() {
				alert('Error reaching server. Try again later.');
			}
		});
	}

	function deleteIt(that, id) {
		if(confirm("Are you sure you want to delete this role?")){
			$.ajax({
				url:'/backoffice/roles/delete/'+id,
				dataType:'json',
				success:function(data) {
					$(that).parents('tr').fadeOut('fast');
					alert(data.success);
				},
				error:function() {
					alert("Error reaching server, try again later.");
				}
			});
		}
	}
	var expanded = false;

	function showCheckboxes() {
		var checkboxes = document.getElementById("checkboxes");
		if (!expanded) {
			checkboxes.style.display = "block";
			expanded = true;
		} else {
			checkboxes.style.display = "none";
			expanded = false;
		}
	}
</script>

@endsection
