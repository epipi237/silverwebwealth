@extends('layouts.app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1"></div>
    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-10" style="padding:18% 5%;">
      <div class="col-xs-6 col-sm-4 col-md-12 col-lg-12 text-center" style="background-color:white;color:black;">
        <br><br><br><br><br>
        <b>Cannot exceed more than 4 users!</b>
        <p>Please contact your referrer: <b>{{$user->name}}</b></p>
        <div class="row">
          <p>Phone: <b>{{$user->phone}}</b></p>
          <p>E-mail: <b>{{$user->email}}</b></p>
        </div>
        <br><br><br><br><br>
      </div>
    </div>
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-1"></div>
  </div>
</div>

@stop