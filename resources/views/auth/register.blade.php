@extends('layouts.app')
@section('head')
<style type="text/css">
    @import url('https://fonts.googleapis.com/css?family=Merriweather');
</style>
<title>Register - WebWealthAfrica</title>
@endsection
@section('content')
<marquee HSPACE=10 WIDTH="100%">
    <div id="marqueeText" style="color:red; padding-top: 15px; font-weight:bold;font-size:15px; font-family: 'Merriweather', serif;"></div>
</marquee>
<form class="login-form" role="form" method="POST" action="{{ url('register/'.$user->name) }}">
    {{ csrf_field() }}

    <div class="login-wrap">
        @if(isset($errors))
        @foreach ($errors->all() as $error)
        <span class="help-block text text-danger">
            <strong>{{ $error }}</strong>
        </span>
        @endforeach
        @endif
        @if (Session::has('auth_message'))
        <span class="help-block text text-danger">
            <strong>{{ Session::get('auth_message') }}</strong>
        </span>
        @endif
        <p class="login-img"><i class="icon_lock_alt"></i> @lang('auth.register.title')</p>
        <div class="input-group">
            <span class="input-group-addon">@lang('auth.register.sponsor')</span>
            <input type="text" disabled="" readonly="" class="form-control disabled" value="{{$user->name}}">
        </div>
        <input type="hidden" name="sponsor" value="{{$user->name}}">

        <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="icon_profile"></i></span>
            <input type="text" class="form-control" required="" value="{{old('name')}}" placeholder="@lang('auth.name_placeholder')" name="name" >
        </div>

        <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="icon_profile"></i></span>
            <input type="email" class="form-control" value="{{old('email')}}" placeholder="@lang('auth.email_placeholder')" name="email" >
        </div>

        <div class="input-group {{ $errors->has('phone') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
            <input type="tel" class="form-control" required="" value="{{old('phone')}}" placeholder="@lang('auth.register.phone')" name="phone" >
        </div>

        <div class="input-group {{ $errors->has('country') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="fa fa-country"></i></span>
            <select class="form-control" id="country" name="country" required="required" onchange="getTowns(this)">
                <option value="">Select your country</option>
                @foreach (\App\Country::all() as $country)
                <option value="{{$country->id}}">{{$country->name}} </option>
                @endforeach
            </select>
        </div>

        <div class="input-group {{ $errors->has('town') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="fa fa-town"></i></span>
            <select class="form-control" id="town" name="town" required="required">
                <option value="">Select your town</option>
            </select>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
            <input type="password" name="password" class="form-control" placeholder="@lang('auth.password')" required="required">
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
            <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('auth.password')" required="required">
        </div>
        
        <button class="btn btn-primary btn-lg btn-block register-btn" type="submit">@lang('auth.signup_text')</button>
        <a href="{{url('login')}}" class="btn btn-info btn-lg btn-block">@lang('auth.login_text')</a>
        @lang('auth.register.more_details_text')<a target="_blank" href="http://blog.webwealthafrica.com" style="color: blue!important;">@lang('auth.register.here')</a>
    </div>
</form>
@endsection

@section('foot')
<script type="text/javascript">
    $(function() {
      $('.register-btn[type="submit"]').click(function (evt) {
        evt.preventDefault();
        var form = $(this).parents('form.login-form');
        $username = $('input[name="name"]').val();
        if ($username.indexOf(' ') !== -1) {
            $pos = $username.indexOf(' ');console.log($pos + " " + $username.length);
            $text = $pos === 0? " at the beginning ": $pos === ($username.length-1)? " at the end of your name ": " between letter " + $pos + " and " + ($pos + 1);
            swal('Please clear wide spaces!', 'No spaces are allowed for name. Space found'+$text+'.', 'warning');
            return false;
        }
        $(form).submit();
        
    });
      function formatState (state) {
        if (!state.id) { return state.text; }
        var $state = $(
            '<span><img src="'+ state.title +'" height="40px" width="40px"/> ' + state.text + '</span>'
            );
        return $state;
    }

    $("#packages").select2({
        templateResult: formatState,
        minimumResultsForSearch:5
    });

});
    $(function() {
        $.get('{{route('recently_registered')}}', function (response) {
            var html = "";
            for (var i in response) {
                html += "<span class='' style='padding: 0 25px'><img src='/img/avatar.png' style='marging-right: \'1px\' ' class='img img-rounded' height='15px' width='15px' > "+response[i]['firstname'].charAt(0).toUpperCase()+response[i]['firstname'].slice(1)+"      " + response[i]['lastname'].charAt(0).toUpperCase() + response[i]['lastname'].slice(1) + "</span>";
            }
            $('#marqueeText').html(html);
        });
    });
    function getTowns (that) {
        $("#town").html("<option value=''> Loading Towns... </option>");
        $.ajax({
            url : '/getTowns/' + that.value,
            dataType : 'html',
            success: function(data) {
                $("#town").html("");
                $("#town").html(data);
            },
            error:function() {
                swal("", "Error reaching server, try again later.", "warning");
                //alert();
            }
        });
    };

</script>
@endsection
