@extends('layouts.app')

@section('head')
    <style type="text/css">

        @keyframes slide {
        from { left: 80%;}
        to { left: -80%;}
        }
        @-webkit-keyframes slide {
        from { left: 10%;}
        to { left: -10%;}
        }

        #marquee { 
        color:red; 
        background: rgba(220, 200, 142,0.1);
        width:100%;
        height:40px;
        line-height:40px;
        overflow:hidden;
        position:relative;
        }

        #marqueeText {
        position:absolute;
        top:0;
        left:0;
        width:100%;
        height:120px;
        font-size:20px;
        animation-name: slide;
        animation-duration: 10s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        -webkit-animation-name: slide;
        -webkit-animation-duration: 10s;
        -webkit-animation-timing-function:linear;
        -webkit-animation-iteration-count: infinite;
        }
    </style>
@section('content')
<div id="marquee">
	<div id="marqueeText"></div>
</div>
<form class="login-form" role="form" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="login-wrap">
        @if ($errors->has('name'))
            <span class="help-block text text-danger">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        @if (Session::has('auth_message'))
            <span class="help-block text text-danger">
                <strong>{{ Session::get('auth_message') }}</strong>
            </span>
        @endif
        <p class="login-img"><i class="icon_lock_alt"></i> @lang('auth.login.title')</p>
        <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <span class="input-group-addon"><i class="icon_profile"></i></span>
            <input type="text" class="form-control" required="" value="{{old('name')}}" placeholder="@lang('auth.name_placeholder')" name="name" autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
            <input type="password" name="password" class="form-control" placeholder="@lang('auth.password')">
        </div>
        <label class="checkbox">
            <input type="checkbox" {{ old('remember') ? 'checked' : '' }} value="remember"> @lang('auth.login.remember')
            <span class="pull-right"> <a href="{{ url('password/request') }}"> @lang('auth.login.forgot')?</a></span>
        </label>
        <button class="btn btn-primary btn-lg btn-block" type="submit">@lang('auth.login_text')</button>
        <a target="_blank" href="http://blog.webwealthafrica.com/2017/07/04/the-silver-regime/" class="btn btn-success btn-lg btn-block" type="submit" style="color:white!important;">@lang('auth.more_info_text')</a>
    </div>
</form>

@endsection
@section('foot')
    <script type="text/javascript">
        $(function() {
            $.get('{{route('recently_registered')}}', function (response) {
                var html = "";
                for (var i in response) {console.log(response[i])
                    html += "<span class='' style='padding: 0 25px'>"+response[i]['firstname'].charAt(0).toUpperCase()+response[i]['firstname'].slice(1)+"      " + response[i]['lastname'].charAt(0).toUpperCase() + response[i]['lastname'].slice(1) + "</span>";
                }
                $('#marqueeText').html(html);
            });
        });
    </script>
@endsection
