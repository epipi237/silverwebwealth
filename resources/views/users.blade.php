@extends('layouts.admin')
@section('head')
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Kumar+One');
        @import url('https://fonts.googleapis.com/css?family=Metamorphous');

        .alert-success {
            background: #2fbe35;
            color: #fff;
            text-align: center;
            font-weight: bolder;
            font-family: 'Kumar One', cursive;
        }

        .panel-heading {
            font-family: 'Metamorphous', cursive;
        }
    </style>
@endsection
@section('content')
    <!--overview start-->
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row">
            <div class="col-xs-1 col-sm-2 col-md-3 col-lg-3"></div>
            <div class="col-xs-10 col-sm-8 col-md-6 col-lg-6">
                <div class="users-alert alert alert-success"
                     style="display: none;"></div>
            </div>
            <div class="col-xs-1 col-sm-2 col-md-3 col-lg-3"></div>
        </div>

        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading " style="height: 25%">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        Users Records ({{ $users->count() }} of {{ $users->total() }})
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <form action="{{ route('search_users') }}" method="GET">
                            <div class="form-group" style="margin-bottom: 0;">
                                <div class="input-group">
                                    <input type="text" class="form-control"
                                           value="{{ isset($search)?$search:null }}" name="search"
                                           id="user_search_field"
                                           placeholder="Search users"
                                           onkeyup="if (event.which === 13) $(this).parents('form').submit()">
                                    <span class="input-group-addon"><i class="fa fa-search" id="search_button"
                                                                       onclick="$(this).parents('form').submit()"
                                                                       style="font-size: 18px; color: #4377c5;"></i></span>
                                </div>
                            </div>
                        </form>
                    </div>
                </header>

                <table class="table table-striped table-advance table-hover">
                    <tbody>
                    <tr>
                        <th><i class="icon_profile"></i> Username</th>
                        <th><i class="icon_pin_alt"></i> Paid</th>
                        <th><i class="icon_calendar"></i> Date</th>
                        <th><i class="icon_calendar"></i> Status</th>
                        <th><i class="icon_cogs"></i> Action</th>
                        <th><i class="fa fa-lock"> Reset</i></th>
                        <th><i class="icon_profile"></i> Profile</th>
                        <th><i class="fa fa-shopping-cart"></i> Collected</th>
                        <th><i class="icon_profile"></i> Pay</th>
                    </tr>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            {{-- <td>{ $user->email }}</td> --}}
                            <td>{!! $user->paid == "yes"? "<i class='fa fa-check'></i>":"<i class='icon_close'></i>"   !!}</td>
                            <td>{{ date('D M d, Y', strtotime($user->created_at)) }}</td>
                            <td>{{ strtoupper($user->status) }}</td>
                            <td>
                                <div class="btn-group user-action-bar" data-user="{{$user->id}}">
                                    @if($user->status !== "active")<a data-user="{{$user->id}}" data-type="active"
                                                                      class="btn btn-success user-action" href="#"
                                                                      data-placement="left" data-toggle="tooltip"
                                                                      title="Activate"><i
                                                class="icon_check_alt2"></i></a>@endif
                                    @if($user->status !== "inactive")<a data-user="{{$user->id}}" data-type="inactive"
                                                                        class="btn btn-warning user-action" href="#"
                                                                        data-placement="left" data-toggle="tooltip"
                                                                        title="Deactivate"><i
                                                class="fa fa-ban"></i></a>@endif
                                    @if($user->status !== "suspended")<a data-user="{{$user->id}}" data-type="suspended"
                                                                         class="btn btn-danger user-action" href="#"
                                                                         data-placement="left" data-toggle="tooltip"
                                                                         title="Suspend"><i
                                                class="icon_close_alt2"></i></a>@endif
                                </div>
                            </td>
                            <td><button class="btn btn-danger reset-password" data-name="{{$user->name}}" data-id="{{$user->id}}"> <i class="fa fa-lock"></i></button></td>
                            <td><a href="{{route('show_profile', [$user->id])}}" data-toggle="tooltip" title="View Profile" class="btn btn-warning"><i class="fa fa-eye"></i></a></td>
                            <td align="center"><a href="#" data-user-id="{{$user->id}}" data-toggle="tooltip" title="toggle Status" class="btn btn-sm btn-{{$user->package_collected?'success':'info'}} package-status-info">{{$user->package_collected? "Yes":"No"}}</a></td>
                            <td><a href="{{route('get_add_payment', [$user->id])}}" data-toggle="tooltip" title="Add Payment" class="btn btn-primary"><i class="fa fa-money"></i></a></td>
                      </tr>
                    @endforeach
                    <tr>
                        <td colspan="8">{{$users->links()}}</td>
                    </tr>
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@stop
@section('foot')
    <script type="text/javascript">
        $(function () {
            $('.user-action').click(function (evt) {
                evt.preventDefault();
                updateUserStatus(this);
            });
            $('.reset-password').click(function (evt) {
                evt.preventDefault();
                var $that = this;
                swal({
                        title: "Are you sure?",
                        text: "User won't be able to login without the default password!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, reset am!",
                        cancelButtonText: "No, cancel am!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm){
                        if (isConfirm) {
                            $.ajax('{{route('reset_user_password')}}', {
                                method: 'GET',
                                data: {
                                    _token: '{{csrf_token()}}',
                                    user_id: $($that).attr('data-id'),
                                    user_name: $($that).attr('data-name')
                                },
                                success: function (response) {
                                    swal("Reset!", "Password Reset Successfully. Password set to `"+response.password+"`", "success");
                                },
                                error: function (error) {
                                    swal("Oops", "Something went wrong!", "error");
                                }
                            });

                        } else {
                            //
                        }
                    });
            });
            $('[data-toggle="tooltip"]').tooltip();
            $('.package-status-info').click(function(evt) {
                var $user_id = $(this).attr('data-user-id');
                var $that = this;
                $('.users-alert').removeClass('alert-danger')
                            .removeClass('alert-success').html("").fadeOut(300);
                if ($(this).text() == "Yes") {
                    swal({
                        title: "Warning",
                        text: "Revert user package status to `Not collected`?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Sure",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function(isConfirm){
                        if (isConfirm) {
                            togglePackageCollectedStatus($user_id, $that);
                        }
                    });
                    return false;
                }
                togglePackageCollectedStatus($user_id, $that);
            });
        });
        function togglePackageCollectedStatus ($user_id, that) {
            var $alert = $('.users-alert');
            $.post(
                'users/toggle_package_collected/'+$user_id, 
                {_token: '{{csrf_token()}}'}, 
                function (response) {
                    if (response.status === "success") {
                        var btnClass = response.state===1? "btn-success":"btn-info";
                        $(that).html(response.state===1?"Yes":"No").removeClass('btn-info btn-success').addClass(btnClass);

                        $($alert).removeClass('alert-danger')
                            .addClass('alert-success').html(response.message)
                            .show(300).delay(9000, function (elt) {
                                $('.users-alert').fadeOut(300);
                            });
                    } else {
                        $($alert).removeClass('alert-success')
                            .addClass('alert-danger').html(response.message)
                            .fadeIn(300).delay(50000, function () {
                            $(this).hide(300);
                        });
                    }
                });
        }
        function updateUserStatus($that) {
            var $user_id = $($that).attr('data-user');
            if ($user_id !== $($that).parent().attr('data-user')) return;
            var $type = $($that).attr('data-type');
            var $alert = $('.users-alert');
            $.ajax('{{ route('change_user_status') }}', {
                method: "POST",
                cache: false,
                data: {
                    _token: "{{csrf_token()}}",
                    type: $type,
                    user_id: $user_id
                },
                success: function (response) {
                    if (response.status === "success") {
                        var $actions = "";
                        var $status = response.user_status;
                        var $status_text = $status.toUpperCase();
                        var $user_id = response.user_id;
                        if ($status !== "active") {
                            $actions += '<a data-user="' + $user_id + '" data-type="active"' +
                                ' class="btn btn-success user-action" href="#" ' +
                                ' data-placement="left" data-toggle="tooltip" ' +
                                ' title="Activate"><i ' +
                                ' class="icon_check_alt2"></i></a>';
                        }
                        if ($status !== "inactive") {
                            $actions += '<a data-user="' + $user_id + '" data-type="inactive" ' +
                                'class="btn btn-warning user-action" href="#" ' +
                                ' data-placement="left" data-toggle="tooltip" ' +
                                ' title="Deactivate"><i ' +
                                ' class="fa fa-ban"></i></a>';
                        }
                        if ($status !== "suspended") {
                            $actions += '<a data-user = "' + $user_id + '" ' +
                                ' data-type="suspended" ' +
                                ' class="btn btn-danger user-action" ' +
                                ' href="#" data-placement="left" ' +
                                ' data-toggle="tooltip" title="Suspend" > <i ' +
                                ' class="icon_close_alt2" > </i> </a> ';
                        }
                        $($that).parents('.user-action-bar').html($actions)
                            .find('.user-action').bind('click', function (evt) {
                            evt.preventDefault();
                            updateUserStatus(this);
                        }).end().parent('td').prev().html($status_text);

                        $($alert).removeClass('alert-danger')
                            .addClass('alert-success').html(response.message)
                            .show(300).delay(9000, function (elt) {
                            $('.users-alert').fadeOut(300);
                        });
                        $('[data-toggle="tooltip"]').tooltip();
                    } else {
                        $($alert).removeClass('alert-success')
                            .addClass('alert-danger').html(response.message)
                            .fadeIn(300).delay(3000, function () {
                            $(this).hide(300);
                        });
                    }
                },
                error: function (error) {

                }
            });
        }
    </script>
@stop
