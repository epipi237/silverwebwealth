<div id="sidebar"  class="nav-collapse ">
  <!-- sidebar menu start-->
  <ul class="sidebar-menu">
    <li class="active">
      <a class="" href="/backoffice/profile/{{$user->id}}">
        <i class="icon_house_alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="active">
      <a class="" href="/backoffice/payment/{{$user->id}}">
        <i class="fa fa-money"></i>
        <span>Make payment</span>
      </a>
    </li>
    <li class="active">
      <a class="" href="/backoffice/widthraw/">
        <i class="fa fa-money"></i>
        <span>Make Withdrawal</span>
      </a>
    </li>

    <li class="sub-menu">
      <a href="javascript:;" class="">

        <i class="icon_documents_alt"></i>
        <span>Profile</span>
        <span class="menu-arrow arrow_carrot-right"></span>
      </a>
      <ul class="sub">
        <li><a class="" href="{{route('show_profile', \Auth::User()->id)}}">Profile</a></li>
        <li><a class="" href="{{ route('show_profile', \Auth::User()->id) }}/#edit-profile">Edit Profile</a></li>
        <li><a class="" href="{{ route('show_profile', \Auth::User()->id) }}#recent-activity">View Transactions</a></li>
        @role('administrator, superadministrator')
        <li><a class="" href="{{ route('show_profile', \Auth::User()->id) }}#view-roles">View Role</a></li>
        @endrole
        <li><a class="" href="{{ route('show_profile', \Auth::User()->id) }}#view-downline">View Downlines</a></li>

        @role('superadministrator', 'administrator')
        <li><a class="" href="{{ route('show_profile', \Auth::User()->id) }}#view-upline">View Uplines</a></li>
        @endrole
        <li><a class="">ID:00{{$user->id}}</a></li>
        <li class=""><a class="" href="{{ url('/logout') }}"><span>Logout</span></a></li>
      </ul>
    </li>

    @role('superadministrator', 'administrator')
    <li class="sub-menu">
      <a href="javascript:;" class="">
        <i class="icon_document_alt"></i>
        <span>Users</span>
        <span class="menu-arrow arrow_carrot-right"></span>
      </a>
      <ul class="sub">
        <li><a class="" href="{{ route('user_index') }}">Manage</a></li>
      </ul>
    </li>
    @endrole

    @role('superadministrator', 'administrator')
    <li class="sub-menu">
      <a href="javascript:;" class="">

        <i class="icon_desktop"></i>
        <span>Roles</span>
        <span class="menu-arrow arrow_carrot-right"></span>
      </a>
      <ul class="sub">
        <li><a class="" href="{{route('roles-index')}}">All Roles</a></li>
        <li><a class="" href="{{route('permissions-index')}}">All Permissions</a></li>
      </ul>
    </li>
    @endrole

    {{--<li>
    <a class="" href="">
      <i class="icon_piechart"></i>
      <span>Charts</span>
    </a>
  </li>--}}

</ul>
<!-- sidebar menu end-->
</div>
