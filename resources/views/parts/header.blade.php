<div class="top-nav notification-row">                
    <!-- notificatoin dropdown start-->
    <ul class="nav pull-right top-menu">

        <!-- task notificatoin start -->
        <li id="task_notificatoin_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                <i class="icon-task-l"></i>
                <span class="badge bg-important"></span>
            </a>
            <ul class="dropdown-menu extended tasks-bar">
                <div class="notify-arrow notify-arrow-blue"></div>
                <li>
                    <p class="blue"></p>
                </li>
                <li class="external">
                    <a href="javascript:;">See All Tasks</a>
                </li>
            </ul>
        </li>
        <!-- task notificatoin end -->

        <!-- inbox notificatoin start-->
        <li id="mail_notificatoin_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                <i class="icon-envelope-l"></i>
                <span class="badge bg-important"></span>
            </a>
        </li>
        <!-- inbox notificatoin end -->

        <!-- alert notification start-->
        <li id="alert_notificatoin_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
                <i class="icon-bell-l"></i>
                <span class="badge bg-important"></span>
            </a>
            <ul class="dropdown-menu extended notification">
                <div class="notify-arrow notify-arrow-blue"></div>
                <li>
                    <p class="blue"></p>
                </li> 
                <li>
                    <a href="javascript:;">See all notifications</a>
                </li>
            </ul>
        </li>
        <!-- alert notification end-->
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="/#">
                <!-- <span class="profile-ava">
                    <img alt="" src="/img/avatar.jpg">
                </span> -->
                <span class="username">{{ ucfirst(\Auth::user()->name) }}</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <div class="log-arrow-up"></div>
                <li class="eborder-top">
                    <a href="{{ route('show_profile', Auth::user()->id) }}"><i class="icon_profile"></i> My Profile</a>
                </li>
                <li>
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!-- notificatoin dropdown end-->
</div>