<div class="container">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-md-offset-2" id="makePayment">
			<h3 class="c-font-24 c-font-sbold">@lang('page.donate.momo_title')</h3>
			<p>@lang('page.donate.momo_message').</p>
			<form class="form-horizontal" role="form" method="POST" onsubmit="makePayment(event)" action="/mobile_money_cameroon/sendpayment">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="auth-email" class="hide">@lang('page.donate.momo_number')</label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon3">+237</span>
						<input class="form-control input-lg c-square" required="" name="phone" placeholder="@lang('page.clients.phone_placeholder')" >
					</div>
				</div>
				<div class="form-group">
					<label for="auth-password" class="hide">@lang('page.donate.momo_amount')</label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon3">{{$donor->country->currency}}</span>
						<input type="number" class="form-control input-lg c-square" required="" name="amount" placeholder="@lang('page.donate.momo_amount_placeholder')">
					</div>
				</div>
				<div class="form-group">
					<div class="btn-group btn-block margin-bottom-20 ">
						<button type="submit" class="btn btn-main btn-donate btn-lg btn-block custom-rounded">
							@lang('page.donate_now')
						</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-4 col-sm-12 col-md-offset-2" id="afterPayment">

		</div>
	</div>
</div>
<script type="text/javascript">
	var isOk=false
	var var1='';
	window.has_paid = 0;
	var $loader = $(".wrap-loader");

	function setter(PaymentID){
		var new_data={
			payment_id:PaymentID
			_token:{{csrf_token()}}
		}

		$loader.removeClass('hide');

		$.ajax({
			url:"/mobile_money_cameroon/finalpayment",
			type: 'POST',
			data:new_data,
			dataType:'json',
			success: function(data) {
				if(data.status=='success'){
					window.has_paid=1;
						$loader.addClass('hide');
						$('#makePayment').addClass('hide');
						$('#afterPayment').html('Your contribution was successfully recieved. We thank you for contributing to this cause.');
						donateComplete(data.transaction_id);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					//alert(errorThrown);
					$loader.addClass('hide');
					$('#afterPayment').html("{{ trans('page.donate.momo_donate_error0') }}.");
				},
			}); 
	}


	function checker(PaymentID){
		$loader.removeClass('hide');

		$.ajax({
			url:"/mobile_money_cameroon/checker/"+PaymentID,
			type: 'GET',		
			success: function(data) {			
				var array3=data.split("|")
				console.log(array3);
				if(array3[0]==1){	
					clearInterval(var1);					
					isOk=true;
					if(window.has_paid==0){
						setter(PaymentID);	
					}	

					window.has_paid=1;								       
					return false;       
				}
				else{
					if(array3[0]==4 || array3[0]==3 ){
						checker(PaymentID)
					}
					if(array3[0]==5){
						if(isOk==true) 	{
							clearInterval(var1); 
							return
						}	     			
						isOk=true
		     			// $.alert({
			     		// 	text:'Alert!',
			     		// 	content:"<div class='panel alert'><div class='panel-body'>Votre paiement n'a pas ete effectue due aux raisons suivantes <br><ul style='list-style:none'><li>Votre numero de telephone n'est pas un compte mobile money valid.</li><li>Vous n'avez pas assez de credit dans votre compte Mobile Money</li></ul></div></div>"
			     		// });	   
			     		alert("{{ trans('page.donate.momo_donate_error1') }}.");
			     	}
			     }						

			 },
			 error: function (jqXHR, textStatus, errorThrown) {
					//alert(errorThrown);
				},
			});

	}

	function makePayment(e){
		e.preventDefault();
		that = e.target;
		isOk=false;
		window.has_paid = 0;
		$loader.removeClass('hide');
		$.ajax({
			url: that.action,
			type: 'GET',
			data: $(that).serialize(),	
			success: function(data) {

				console.log(data);
				if(data.indexOf('pending')!=-1){			
					return;
				}
				
				var array=data.split(",")
				var array2=array[1].split("=")
				var PaymentID=Number(array2[1])

				if(array[0]==1){
					var1=setInterval("checker("+PaymentID+")",2000);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("{{ trans('page.error_reaching_server') }}.");
			}		

		});
	}
</script>