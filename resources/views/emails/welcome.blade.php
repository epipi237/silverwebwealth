<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Web Wealth Africa >> Redefining the power of a network">
  <meta name="author" content="Web Wealth Africa Team">
  <meta name="keyword" content="Web, Wealth, Africa, Network, Marketing, Scheme">
  <link rel="shortcut icon" href="{{URL::to('images')}}/logo.png">

  <title>{{ config('app.name', 'Web Wealth Africa') }}</title>

  <!-- Fonts -->
  <link rel="icon" type="image/png" href="{{URL::to('images')}}/logo.png">

  <style type="text/css">
    body{
      margin: 0px;
      padding: 0px;
      font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
      font-size: 14px;
      line-height: 1.42857143;
      color: #333;
    }

    .head {
      margin: 0;
      padding: 0;
      overflow: hidden;
      background-color: #333;
    }

    .logo{
      display: inline-block;
      float: left;
      padding: 10px;
      color: #ffffff!important;
    }

    ul{
      list-style-type: none;
      display: inline-block;
      float: right;
    }

    li {
      float: left;
    }

    a {
      display: inline;
      text-align: center;
      text-decoration: none;
      color: #000000;
    }

    li a{
      color: #ffffff;
      padding: 10px 10px;
    }

    /* Change the link color to #111 (black) on hover */
    li a:hover {
      background-color: #D9534F !important;
      color: #cccccc;
    }

    p a{
      padding: 0px!important;
      margin: 0px!important;
      color: #D9534F!important;
    }

    p a:hover{
      color: #D9534F !important;
      padding: 0px;
    }

    h3{
      color: black;
      margin: 2%!important;
    }

    span.user-name{
      padding: 0px!important;
      margin: 0px!important;
      color: #D9534F!important;
    }

    img.img-sample{
      width: 95%; 
      height: 30%; 
      margin: 1%;
    }

    .p_text {
      color: black;
      margin: 2%;
    }
  </style>
</head>

<body>
  <div class="head">
    <span class="logo">
      <a href="{{url('/')}}">
        <img src="{{URL::to('images')}}/logo.png" style="width: 40%; height: 60%;" /><br>
        <span style="color: #ffffff!important;">Web Wealth Africa</span>
      </a>
    </span>

    <span style="width: 60%;">
      <ul>
        <li><a href="{{url('/')}}"> @lang('profile.home') </a></li>
        <li><a target="_blank" href="mailto:info@webwealthafrica.com"> @lang('profile.contact_us') </a></li>
      </ul>
    </span>
  </div>

  <p class="p_text">

    <h3> 
      @lang('profile.hello') <span class="user-name">{{$name}}</span>,  
    </h3>

  </p>

  <p class="p_text">

    @lang('profile.welcome_msg_part')

    <br><br>

    @lang('profile.best_regards'),
    <br>
    @lang('profile.the_team').

  </p>

</body>

</html>
