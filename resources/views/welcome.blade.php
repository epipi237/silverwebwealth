@extends('layouts.app')

@section('content')

<div class="collapse navbar-collapse" id="app-navbar-collapse">
    <!-- Left Side Of Navbar -->
    <ul class="nav navbar-nav">
        &nbsp;
    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="nav navbar-nav navbar-right">
        <!-- Authentication Links -->
        @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Login</a></li>
        @else
        <li>
            <a href="{{ route('backoffice') }}" >
                {{ Auth::user()->name }}
            </a>
        </li>
        <li>
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
        @endif
    </ul>
</div>

<div class="flex-center position-ref full-height">
    <div class="content" style="font-weight: bold;">
        <div class="title m-b-md">
            {{ config('app.name') }}
        </div>
        <div class="small text-info">
            Create your fortune on our network
        </div>

    </div>
</div>

@endsection
