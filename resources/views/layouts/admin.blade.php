<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Web Wealth Africa >> Redefining the power of a network">
    <meta name="author" content="Web Wealth Africa Team">
    <meta name="keyword" content="Web, Wealth, Africa, Network, Marketing, Scheme">
    <link rel="shortcut icon" href="/img/favicon.png">

    <title>@yield('title', "Welcome") >> Web Wealth Africa </title>

    <!-- Bootstrap CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="/css/font-awesome.min.css" rel="stylesheet" />
    <!-- full calendar css-->
    <link href="/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="/assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
    <!-- easy pie chart-->
    <link href="/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="/css/owl.carousel.css" type="text/css">
	<link href="/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
	<link rel="stylesheet" href="/css/fullcalendar.css">
	<link href="/css/widgets.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/style-responsive.css" rel="stylesheet" />
	<link href="/css/xcharts.min.css" rel=" stylesheet">
	<link href="/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
	<link href="/assets/sweetalert/sweetalert.css" rel="stylesheet">
  <link href="/assets/select2/css/select2.css" rel="stylesheet" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
      @yield('head')
      <style type="text/css">
        .hide{
          display: none;
        }
      </style>
  </head>

  <body>
  <!-- container section start -->
  <section id="container" class="">

      <header class="header dark-bg">
            <div class="toggle-nav">
                <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
            </div>

            <!--logo start-->
            <a href="{{route('show_profile', \Auth::user()->id)}}" class="logo">Web<span class="lite">Wealth</span></a>
            <!--logo end-->

            <div class="nav search-row" id="top_menu">
                <!--  search form start -->
                <ul class="nav top-menu">
                    <li>
                        <form class="navbar-form">
                            <input class="form-control" id="search" placeholder="Search" type="text">
                        </form>
                    </li>
                </ul>
                <!--  search form end -->
            </div>

            @include('parts.header')
      </header>
      <!--header end-->

      <!--sidebar start-->
      <aside>
          @include('parts.sidebar')
      </aside>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="text-center">
                      @if(Session::has('success'))
                          @component('inc.success')
                              {{ Session::get('success') }}
                          @endcomponent
                      @endif
                      @if(Session::has('info'))
                          @component('inc.info')
                              {{ Session::get('info') }}
                          @endcomponent
                      @endif
                      @if(Session::has('warning'))
                          @component('inc.warning')
                              {{ Session::get('warning') }}
                          @endcomponent
                      @endif
                      @if(Session::has('danger'))
                          @component('inc.danger')
                              {{ Session::get('danger') }}
                          @endcomponent
                      @endif
                  </div>
              </div>
            @yield('content')
          </section>
          <div class="text-right"></div>
      </section>
  </section>
  <!-- container section start -->

  <!-- container section start -->

  <!-- javascripts -->
  <script type="text/javascript" src="/js/jquery.js"></script>
  <script type="text/javascript" src="/js/jquery-ui-1.10.4.min.js"></script>
  <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="/js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script type="text/javascript" src="/js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script type="text/javascript" src="/js/jquery.scrollTo.min.js"></script>
  <script type="text/javascript" src="/js/jquery.nicescroll.js"></script>
  <!-- charts scripts -->
  <script type="text/javascript" src="/assets/jquery-knob/js/jquery.knob.js"></script>
  <script type="text/javascript" src="/js/jquery.sparkline.js"></script>
  <script type="text/javascript" src="/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script type="text/javascript" src="/js/owl.carousel.js" ></script>
  <!-- jQuery full calendar -->
  <<script type="text/javascript" src="/js/fullcalendar.min.js"></script> <!-- Full Google Calendar - Calendar -->
  <script type="text/javascript" src="/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
  <!--script for this page only-->
  <script type="text/javascript" src="/js/calendar-custom.js"></script>
  <script type="text/javascript" src="/js/jquery.rateit.min.js"></script>
  <!-- custom select -->
  <script type="text/javascript" src="/js/jquery.customSelect.min.js" ></script>
  <script type="text/javascript" src="/assets/chart-master/Chart.js"></script>

  <!--custome script for all page-->
  <script type="text/javascript" src="/js/scripts.js"></script>
  <!-- custom script for this page-->
  <script type="text/javascript" src="/js/sparkline-chart.js"></script>
  <script type="text/javascript" src="/js/easy-pie-chart.js"></script>
  <script type="text/javascript" src="/js/jquery-jvectormap-1.2.2.min.js"></script>
  <script type="text/javascript" src="/js/jquery-jvectormap-world-mill-en.js"></script>
  <script type="text/javascript" src="/js/xcharts.min.js"></script>
  <script type="text/javascript" src="/js/jquery.autosize.min.js"></script>
  <script type="text/javascript" src="/js/jquery.placeholder.min.js"></script>
  <script type="text/javascript" src="/js/gdp-data.js"></script>
  <script type="text/javascript" src="/js/morris.min.js"></script>
  <script type="text/javascript" src="/js/sparklines.js"></script>
  <script type="text/javascript" src="/js/charts.js"></script>
  <script type="text/javascript" src="/js/jquery.slimscroll.min.js"></script>
  <script type="text/javascript" src="/assets/sweetalert/sweetalert.min.js"></script>
  <script src="/assets/select2/js/select2.js"></script>
  <script type="text/javascript">

    $(function() {
      //knob
      $(".knob").knob({
        'draw' : function () {
          $(this.i).val(this.cv + '%')
        }
      });

      //carousel
      $("#owl-slider").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true
      });

      //custom select box
      $('select.styled').customSelect();

      /* ---------- Map ---------- */
      $('#map').vectorMap({
        map: 'world_mill_en',
        series: {
          regions: [{
            values: gdpData,
            scale: ['#000', '#000'],
            normalizeFunction: 'polynomial'
          }]
        },
        backgroundColor: '#eef3f7',
        onLabelShow: function(e, el, code){
          el.html(el.html()+' (GDP - '+gdpData[code]+')');
        }
      });
    });

  </script>
  @yield('foot')
  @yield('scripts')

</body>
</html>
