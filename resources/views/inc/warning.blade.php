<div class="alert alert-warning alert-dismissable text-center" style="padding: 0 15px" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h5 style="font-weight: bolder;">{{ $slot }}</h5>
</div>