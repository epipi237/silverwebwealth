<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    "login" => [
        "title" => "Login",
        "remember" => "Remember me",
        "forgot" => "Forgot Password",
    ],
    "register" => [
        "title" => "Register",
        "sponsor" => "Sponsor",
        "phone" => "Phone Number",
        "more_details_text" => "For more information click ",
        "here" => "here"
    ],
    "name_placeholder" => "Username",
    "email_placeholder" => "Email",
    "login_text" => "Login",
    "signup_text" => "Register",
    "password" => "Password",
    "more_info_text" => "More details",

];
