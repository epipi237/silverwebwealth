<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});

Route::get('/test_welcome_email', function () {
	$user = \App\User::find(1);
	$email = $user->email;

	return view('emails.welcome', compact("email"));
});

Route::get('login', 'AuthController@showLogin')->name('login');
Route::post('login', 'AuthController@login')->name('login');

Route::get('register/{name}', "AuthController@showRegister")->name('register');
Route::post('register/{name}', "AuthController@register");

Route::post('logout', "AuthController@logout")->name('logout');
Route::get('logout', "AuthController@logout")->name('logout');

Route::get('/getTowns/{country_id}', 'AuthController@getTowns')->name('get_towns');

Route::get('/backoffice', 'HomeController@index')->name('backoffice');
Route::post('/reset_password', 'AccountsController@resetPassword')->name('reset_password');

Route::get('/recently-registered', function () {
	$mostRecent10 = App\User::where('firstname', '!=', '')->where('lastname', '!=', '')->orderBy('users.created_at', 'desc')->take(10)->get(['name', 'firstname', 'lastname']);
	return response()->json($mostRecent10);
})->name('recently_registered');


	Route::group(['prefix'=>'mobile_money_cameroon'], function(){
		Route::get('sendpayment/{id}','MomoController@makePayment');
		// Route::get('sendpayment/{id}','MomoController@sendpayment'); //old route
		Route::get('checker/{PaymentID}','MomoController@checker');
		Route::post('finalpayment','MomoController@finall');
		Route::get('finalpayment','MomoController@finall');
		Route::post('widthraw','payController@postwidthraw');
	});



Route::group(['prefix'=>"backoffice", "middleware"=>['auth']], function(){
	Route::get('widthraw','payController@withraw');
	Route::post('pay/{id}','payController@pay')->middleware('role:superadministrator');
	Route::get('payment/{userId}','payController@payment');
	Route::group(['prefix'=>"profile"], function(){
		Route::get('/{user_id}', 'AccountsController@show')->name('show_profile')->middleware('permission:read-profile');
		Route::post('/{user_id}', 'AccountsController@update')->name('update_profile')->middleware("permission:update-profile");
	});

	Route::group(['prefix' => 'users', "middleware"=>['auth']], function(){
		Route::name('user_index')->get('/', 'UserController@index')->middleware('role:administrator|superadministrator');
		Route::name('change_user_status')->get('/change_status','UserController@changeStatus');
		Route::name('search_users')->get('/search','UserController@search');
		Route::name('get_add_payment')->get('/add_payment/{id}','payController@getAddPayment');
		Route::name('post_add_payment')->post('/add_payment/{id}','payController@postAddPayment');
		Route::name('reset_user_password')->get('/reset_password','UserController@resetPassword');
		Route::name('toggle_package_collected')->post('/toggle_package_collected/{user_id}','UserController@togglePackageCollected');
	});


	//routes for roles and permissions
	Route::group(['prefix' => 'roles', 'middleware' => ['role:superadministrator']], function(){
		Route::get('/index', 'RolesAndPermissionsController@roleIndex')->name('roles-index');
		Route::get('/create', 'RolesAndPermissionsController@roleCreate')->name('roles-create');
		Route::post('/store', 'RolesAndPermissionsController@roleStore')->name('roles-store');
		Route::get('/edit/{id}', 'RolesAndPermissionsController@roleEdit')->name('roles-edit');
		Route::post('/update/{id}', 'RolesAndPermissionsController@roleUpdate')->name('roles-update');
		Route::get('/delete/{id}', 'RolesAndPermissionsController@roleDelete')->name('roles-delete');
	});

	Route::group(['prefix' => 'permissions', 'middleware' => ['role:superadministrator']], function(){
		Route::get('/index', 'RolesAndPermissionsController@permissionIndex')->name('permissions-index');
		Route::get('/create', 'RolesAndPermissionsController@permissionCreate')->name('permissions-create');
		Route::post('/store', 'RolesAndPermissionsController@permissionStore')->name('permissions-store');
		Route::get('/edit/{id}', 'RolesAndPermissionsController@permissionEdit')->name('permissions-edit');
		Route::post('/update/{id}', 'RolesAndPermissionsController@permissionUpdate')->name('permissions-update');
		Route::get('/delete/{id}', 'RolesAndPermissionsController@permissionDelete')->name('permissions-delete');
	});

	Route::group(['prefix' => 'role-users', 'middleware' => ['role:superadministrator']], function(){
		Route::get('/{id}', 'RolesAndPermissionsController@roleUser')->name('role-users');
		Route::post('/store/{id}', 'RolesAndPermissionsController@roleUserStore')->name('role-user-store');
		Route::get('/delete/{user_id}/{role_id}', 'RolesAndPermissionsController@roleUserDelete')->name('role-user-delete');
	});

});
